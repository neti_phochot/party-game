﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ui
{
    public class HoverButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler
    {
        private enum HoverState
        {
            Size,
            Color,
        }

        [SerializeField] private RectTransform button;
        [SerializeField] private TextMeshProUGUI textMeshPro;
        [SerializeField] private Image image;
        [SerializeField] private HoverState hoverState = HoverState.Size;

        [SerializeField] private Vector2 hoverSize = new Vector2(400, 70);
        [SerializeField] private float hoverDuration = 0.2f;

        [SerializeField] private Vector2 primarySize = new Vector2(300, 60);
        [SerializeField] private float primaryDuration = 0.2f;

        public void OnPointerEnter(PointerEventData eventData)
        {
            EnterButton();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            ExitButton();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            ExitButton();
        }

        private void EnterButton()
        {
            switch (hoverState)
            {
                case HoverState.Size:
                    SizeHoverEnter();
                    break;
                case HoverState.Color:
                    ColorHoverEnter();
                    break;
                default:
                    break;
            }
        }

        private void ExitButton()
        {
            switch (hoverState)
            {
                case HoverState.Size:
                    SizeHoverExit();
                    break;
                case HoverState.Color:
                    ColoHoverExit();
                    break;
                default:
                    break;
            }
        }

        private void SizeHoverEnter()
        {
            button.DOSizeDelta(hoverSize, hoverDuration, false);
        }

        private void SizeHoverExit()
        {
            button.DOSizeDelta(primarySize, primaryDuration, false);
        }

        private void ColorHoverEnter()
        {
            textMeshPro.DOColor(Color.white, 0.2f);
            image.DOColor(Color.black, 0.5f);
        }

        private void ColoHoverExit()
        {
            textMeshPro.DOColor(Color.black, 0.2f);
            image.DOColor(Color.white, 0.5f);
        }
    }
}