﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace ui.mainmenu
{
    public class MainMenuUI : MonoBehaviour, IVisible
    {
        [SerializeField] private Button hostGameButton;
        [SerializeField] private Button findGameButton;
        [SerializeField] private Button quitButton;

        private void Awake()
        {
            Debug.Assert(hostGameButton != null, $"{nameof(hostGameButton)}== null");
            Debug.Assert(findGameButton != null, $"{nameof(findGameButton)}== null");
            Debug.Assert(quitButton != null, $"{nameof(quitButton)}== null");

            quitButton.onClick.AddListener(Application.Quit);

            hostGameButton.onClick.AddListener(() =>
            {
                MainMenuManager.Instance.OpenMenu(MainMenuManager.EMenu.HOSTGAME);
            });
            findGameButton.onClick.AddListener(() =>
            {
                MainMenuManager.Instance.OpenMenu(MainMenuManager.EMenu.SERVERLIST);
            });
        }

        private void Start()
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}