﻿using networking;
using Steamworks;
using TMPro;
using ui.messagebox;
using UnityEngine;
using UnityEngine.UI;

namespace ui.mainmenu
{
    public class JoinWithCodeUI : MonoBehaviour, IVisible
    {
        [Header("UI")] [SerializeField] private Button backButton;

        [Header("JOIN WITH CODE")] [SerializeField]
        private TMP_InputField inviteCodeInput;

        [SerializeField] private Button joinButton;

        private string _inviteCode;

        private void Awake()
        {
            Debug.Assert(backButton != null, $"{nameof(backButton)}== null");
            Debug.Assert(inviteCodeInput != null, $"{nameof(inviteCodeInput)}== null");
            Debug.Assert(joinButton != null, $"{nameof(joinButton)}== null");

            backButton.onClick.AddListener(() =>
            {
                Visible = false;
                MainMenuManager.Instance.OpenMenu(MainMenuManager.EMenu.SERVERLIST);
            });

            joinButton.onClick.AddListener(JoinGame);
            inviteCodeInput.onValueChanged.AddListener(OnInviteCodeInputChanged);
        }

        private void OnEnable()
        {
            MatchManager.OnInviteLobbyFound += OnInviteLobbyFoundEvent;
        }

        private void OnDisable()
        {
            MatchManager.OnInviteLobbyFound -= OnInviteLobbyFoundEvent;
        }

        private void OnInviteLobbyFoundEvent(CSteamID lobbyId)
        {
            if (lobbyId.ToString() == "0")
            {
                Debug.Log($"[{nameof(JoinWithCodeUI)}] Cannot find lobby with {_inviteCode}");
                MessageBoxManager.Instance.ShowText("Invite Code", $"Cannot find lobby with {_inviteCode}");
                return;
            }

            Debug.Log($"[{nameof(JoinWithCodeUI)}] Join game {lobbyId} with invite code: {_inviteCode}");
            MatchManager.Instance.JoinGame(lobbyId);
        }

        private void JoinGame()
        {
            if (_inviteCode == default || _inviteCode.Length != MatchManager.INVITE_CODE_LENGTH)
            {
                Debug.Log($"[{nameof(JoinWithCodeUI)}] Invalid invite code!");
                MessageBoxManager.Instance.ShowText("Invite Code",
                    $"Invalid invite code! ({MatchManager.INVITE_CODE_LENGTH})");
                return;
            }

            Debug.Log($"[{nameof(JoinWithCodeUI)}] Looking for lobby with invite code: {_inviteCode}");
            MatchManager.Instance.FindGame(_inviteCode, 1, ELobbyDistanceFilter.k_ELobbyDistanceFilterClose);
        }

        private void OnInviteCodeInputChanged(string value)
        {
            int maxInviteCodeLength = MatchManager.INVITE_CODE_LENGTH;
            if (value.Length > maxInviteCodeLength)
            {
                value = value.Substring(0, maxInviteCodeLength);
            }

            inviteCodeInput.text = value.ToUpper();
            _inviteCode = inviteCodeInput.text;
        }

        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}