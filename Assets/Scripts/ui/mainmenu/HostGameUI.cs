﻿using networking;
using Steamworks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using utils;

namespace ui.mainmenu
{
    public class HostGameUI : MonoBehaviour, IVisible
    {
        [Header("LOBBY SETTINGS")] [SerializeField]
        private string defaultLobbyName;

        [SerializeField] private ELobbyType defaultLobbyType;
        [Range(2, 40)] [SerializeField] private int defaultMaxPlayer;

        [Header("UI")] [SerializeField] private Button createGameButton;
        [SerializeField] private Button closeButton;

        [Header("LOBBY NAME")] [SerializeField]
        private int maxLobbyNameLength;

        [SerializeField] private TMP_InputField lobbyNameInput;

        [Header("LOBBY TYPE")] [SerializeField]
        private Slider lobbyTypeSlider;

        [SerializeField] private TextMeshProUGUI lobbyTypeText;

        [Header("MAX PLAYER")] [SerializeField]
        private Slider maxPlayerSlider;

        [SerializeField] private TextMeshProUGUI maxPlayerText;

        private MatchManager.LobbySetting lobbySetting;

        private void Awake()
        {
            Debug.Assert(createGameButton != null, $"{nameof(createGameButton)}== null");
            Debug.Assert(closeButton != null, $"{nameof(closeButton)}== null");

            createGameButton.onClick.AddListener(HostGame);
            closeButton.onClick.AddListener(() =>
            {
                Visible = false;
                MainMenuManager.Instance.OpenMenu(MainMenuManager.EMenu.MAINMENU);
            });

            InitDefaultLobbySettings();

            lobbyTypeSlider.onValueChanged.AddListener(OnLobbyTypeChanged);
            maxPlayerSlider.onValueChanged.AddListener(OnMaxPlayerSlider);
            lobbyNameInput.onValueChanged.AddListener(OnLobbyNameInputChanged);
        }

        private void HostGame()
        {
            Debug.Log("Creating lobby...");

            lobbySetting.inviteCode = MatchManager.INVITE_CODE_LENGTH.ToRandomLetter();
            lobbySetting.lobbyName = lobbySetting.lobbyName.Replace("%player%", SteamFriends.GetPersonaName());

            Debug.Log($"---------------------");
            Debug.Log($"Lobby Name: {lobbySetting.lobbyName}");
            Debug.Log($"Lobby Type: {lobbySetting.lobbyType}");
            Debug.Log($"Max Player: {lobbySetting.maxPlayer}");
            Debug.Log($"Invite Code: {lobbySetting.inviteCode}");
            Debug.Log($"---------------------");

            MatchManager.Instance.HostGame(lobbySetting);
        }

        private void InitDefaultLobbySettings()
        {
            lobbySetting = new MatchManager.LobbySetting
            {
                lobbyName = defaultLobbyName,
                lobbyType = defaultLobbyType,
                maxPlayer = defaultMaxPlayer,
                inviteCode = "12345",
            };

            UpdateLobbyTypeText();
            UpdateMaxPlayerText();
        }

        private void OnLobbyNameInputChanged(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                lobbySetting.lobbyName = defaultLobbyName;
                return;
            }

            if (value.Length > maxLobbyNameLength)
            {
                value = value.Substring(0, maxLobbyNameLength);
                lobbyNameInput.text = value;
            }

            lobbySetting.lobbyName = value;
        }

        private void OnLobbyTypeChanged(float value)
        {
            ELobbyType selectedLobbyType = ELobbyType.k_ELobbyTypePublic;

            switch ((int)value)
            {
                case 0:
                    selectedLobbyType = ELobbyType.k_ELobbyTypePrivate;
                    break;
                case 1:
                    selectedLobbyType = ELobbyType.k_ELobbyTypePublic;
                    break;
                case 2:
                    selectedLobbyType = ELobbyType.k_ELobbyTypeFriendsOnly;
                    break;
            }

            lobbySetting.lobbyType = selectedLobbyType;
            UpdateLobbyTypeText();
        }

        private void OnMaxPlayerSlider(float value)
        {
            int maxPlayer = (int)value;
            lobbySetting.maxPlayer = maxPlayer;
            UpdateMaxPlayerText();
        }

        private void UpdateMaxPlayerText()
        {
            maxPlayerText.text = $"{lobbySetting.maxPlayer}";
        }

        private void UpdateLobbyTypeText()
        {
            string lobbyType = "Not supported lobby type";

            switch (lobbySetting.lobbyType)
            {
                case ELobbyType.k_ELobbyTypePrivate:
                    lobbyType = "Private";
                    break;
                case ELobbyType.k_ELobbyTypeFriendsOnly:
                    lobbyType = "Friend Only";
                    break;
                case ELobbyType.k_ELobbyTypePublic:
                    lobbyType = "Public";
                    break;
            }

            lobbyTypeText.text = lobbyType;
        }

        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}