﻿using networking;
using Steamworks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ui.mainmenu
{
    public class ServerListItem : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI lobbyNameText;
        [SerializeField] private TextMeshProUGUI playerCountText;
        [SerializeField] private Button joinButton;

        private CSteamID _lobbyId;

        private void Awake()
        {
            Debug.Assert(lobbyNameText != null, $"{nameof(lobbyNameText)}==null");
            Debug.Assert(playerCountText != null, $"{nameof(playerCountText)}==null");
            Debug.Assert(joinButton != null, $"{nameof(joinButton)}==null");

            joinButton.onClick.AddListener(JoinGame);
        }

        public void SetLobbyID(CSteamID lobbyId)
        {
            _lobbyId = lobbyId;
        }

        public void SetLobbyName(string lobbyName)
        {
            lobbyNameText.text = lobbyName;
        }

        public void SetPlayerCount(int online, int maxPlayer)
        {
            playerCountText.text = $"{online}/{maxPlayer}";
        }

        private void JoinGame()
        {
            Debug.Log($"[{nameof(ServerListItem)}] Joining game... {_lobbyId.ToString()}");
            MatchManager.Instance.JoinGame(_lobbyId);
        }
    }
}