﻿using System;
using TMPro;
using UnityEngine;

namespace ui.mainmenu
{
    public class MainMenuManager : MonoBehaviour
    {
        public enum EMenu
        {
            MAINMENU,
            HOSTGAME,
            SERVERLIST,
            JOINWITHCODE,
        }

        public static MainMenuManager Instance;

        [SerializeField] private TextMeshProUGUI gameVersionText;
        [Header("UI")] [SerializeField] private MainMenuUI mainMenuUI;
        [SerializeField] private HostGameUI hostGameUI;
        [SerializeField] private ServerListUI serverListUI;
        [SerializeField] private JoinWithCodeUI joinWithCodeUI;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;

            Debug.Assert(mainMenuUI != null, $"{nameof(mainMenuUI)}!=null");
            Debug.Assert(hostGameUI != null, $"{nameof(hostGameUI)}!=null");
            Debug.Assert(serverListUI != null, $"{nameof(serverListUI)}!=null");
            Debug.Assert(joinWithCodeUI != null, $"{nameof(joinWithCodeUI)}!=null");

            gameVersionText.text = $"v{Application.version}";
        }

        private void OnEnable()
        {
            OpenMenu(EMenu.MAINMENU);
        }

        public void OpenMenu(EMenu eMenu)
        {
            mainMenuUI.Visible = eMenu == EMenu.MAINMENU;
            hostGameUI.Visible = eMenu == EMenu.HOSTGAME;
            serverListUI.Visible = eMenu == EMenu.SERVERLIST;
            joinWithCodeUI.Visible = eMenu == EMenu.JOINWITHCODE;
        }
    }
}