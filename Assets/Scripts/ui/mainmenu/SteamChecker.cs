﻿using System;
using Steamworks;
using ui.messagebox;
using UnityEngine;
using UnityEngine.UI;

namespace ui.mainmenu
{
    public class SteamChecker : MonoBehaviour, IVisible
    {
        [SerializeField] private Button steamCheckButton;
        [SerializeField] private Transform group;

        private void Awake()
        {
            steamCheckButton.onClick.AddListener(Application.Quit);
        }

        private void Start()
        {
            CheckSteam();
        }

        private void CheckSteam()
        {
            //Source: SteamManager.cs

            bool initialized = SteamAPI.Init();
            if (!initialized)
            {
                MessageBoxManager.Instance.ShowText("Steam",
                    "Steam is required to play this game, Please run Steam and try again!");
                Visible = true;
                return;
            }

            Visible = false;
        }

        public bool Visible
        {
            get => group.gameObject.activeSelf;
            set => group.gameObject.SetActive(value);
        }
    }
}