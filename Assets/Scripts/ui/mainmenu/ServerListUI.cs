﻿using networking;
using Steamworks;
using ui.messagebox;
using UnityEngine;
using UnityEngine.UI;

namespace ui.mainmenu
{
    public class ServerListUI : MonoBehaviour, IVisible
    {
        [Header("UI")] [SerializeField] private Button refreshButton;
        [SerializeField] private Button joinWithCodeButton;
        [SerializeField] private Button closeButton;

        [Header("SERVER LIST")] [SerializeField]
        private int maxSearch;

        [SerializeField] private Transform serverListItemContainer;

        private void Awake()
        {
            Debug.Assert(refreshButton != null, $"{nameof(refreshButton)}== null");
            Debug.Assert(joinWithCodeButton != null, $"{nameof(joinWithCodeButton)}== null");
            Debug.Assert(closeButton != null, $"{nameof(closeButton)}== null");

            refreshButton.onClick.AddListener(() =>
            {
                RefreshServerList();
                MessageBoxManager.Instance.ShowText("Server List", "Server list updated!");
            });

            closeButton.onClick.AddListener(() =>
            {
                Visible = false;
                MainMenuManager.Instance.OpenMenu(MainMenuManager.EMenu.MAINMENU);
            });

            joinWithCodeButton.onClick.AddListener(() =>
            {
                Visible = false;
                MainMenuManager.Instance.OpenMenu(MainMenuManager.EMenu.JOINWITHCODE);
            });
        }

        private void OnEnable()
        {
            MatchManager.OnLobbiesFound += OnLobbiesFoundEvent;

            RefreshServerList();
        }

        private void OnDisable()
        {
            MatchManager.OnLobbiesFound -= OnLobbiesFoundEvent;
        }

        private void RefreshServerList()
        {
            Debug.Log($"[{nameof(ServerListUI)}] Refreshing server list...");
            MatchManager.Instance.FindGame(maxSearch, ELobbyDistanceFilter.k_ELobbyDistanceFilterClose);
        }

        private void OnLobbiesFoundEvent(CSteamID[] lobbies)
        {
            GameObject serverListItemTemplate = serverListItemContainer.GetChild(0).gameObject;

            if (!serverListItemTemplate.activeSelf)
            {
                serverListItemTemplate.SetActive(true);
            }

            for (int i = 0; i < maxSearch; i++)
            {
                if (i >= lobbies.Length)
                {
                    if (i < serverListItemContainer.childCount)
                    {
                        serverListItemContainer.GetChild(i).gameObject.SetActive(false);
                    }

                    break;
                }

                ServerListItem serverListItem = i >= serverListItemContainer.childCount
                    ? Instantiate(serverListItemTemplate, serverListItemContainer).GetComponent<ServerListItem>()
                    : serverListItemContainer.GetChild(i).GetComponent<ServerListItem>();

                CSteamID currentLobbyId = lobbies[i];
                serverListItem.SetLobbyID(currentLobbyId);
                serverListItem.SetLobbyName(SteamMatchmaking.GetLobbyData(currentLobbyId, MatchManager.LOBBY_NAME_KEY));
                serverListItem.SetPlayerCount(
                    SteamMatchmaking.GetNumLobbyMembers(currentLobbyId),
                    SteamMatchmaking.GetLobbyMemberLimit(currentLobbyId));
            }
        }

        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}