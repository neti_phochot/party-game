﻿namespace ui.game
{
    public interface IGameAliveUI : IVisible
    {
        void SetAlive(int alive);
    }
}