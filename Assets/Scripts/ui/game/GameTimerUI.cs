﻿using TMPro;
using UnityEngine;

namespace ui.game
{
    public class GameTimerUI : MonoBehaviour, IGameTimerUI
    {
        [SerializeField] private TextMeshProUGUI timerText;

        public void SetTimer(int time)
        {
            timerText.text = $"{time}";
        }

        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}