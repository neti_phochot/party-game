﻿using TMPro;
using UnityEngine;

namespace ui.game
{
    public class GameInfoUI : MonoBehaviour, IGameInfoUI
    {
        [SerializeField] private TextMeshProUGUI headerText;
        [SerializeField] private TextMeshProUGUI infoText;

        public void SetHeader(string header)
        {
            headerText.text = header;
        }

        public void SetInfo(string info)
        {
            infoText.text = info;
        }

        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}