﻿namespace ui.game
{
    public interface IGameTeamAliveUI: IVisible
    {
        void SetTeam(GameUIManager.TeamPlayerData teamPlayerData);
    }
}