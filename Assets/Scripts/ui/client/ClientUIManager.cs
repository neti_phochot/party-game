﻿using core.entity.player;
using core.entity.player.gamemode;
using Mirror;
using ui.voicechat;
using UnityEngine;

namespace ui.client
{
    public class ClientUIManager : NetworkBehaviour
    {
        private static ClientUIManager _i;

        [SerializeField] private Transform groupTransform;
        [Header("UI")] [SerializeField] private DisplayNameUI displayNameUI;
        [SerializeField] private VoiceChatUI voiceChatUI;

        private void Awake()
        {
            _i = this;
        }

        private void OnEnable()
        {
            Player player = GetComponent<Player>();
            player.OnPlayerDisplayNameChanged += OnPlayerDisplayNameChangedEvent;
            player.OnPlayerGameModeChanged += OnPlayerGameModeChangedEvent;

            PlayerVoiceChatHandler playerVoiceChatHandler = GetComponent<PlayerVoiceChatHandler>();
            playerVoiceChatHandler.OnVoiceLevelChanged += OnVoiceLevelChangedEvent;

            groupTransform.gameObject.SetActive(true);
        }

        private void OnDisable()
        {
            Player player = GetComponent<Player>();
            player.OnPlayerDisplayNameChanged -= OnPlayerDisplayNameChangedEvent;
            player.OnPlayerGameModeChanged -= OnPlayerGameModeChangedEvent;

            PlayerVoiceChatHandler playerVoiceChatHandler = GetComponent<PlayerVoiceChatHandler>();
            playerVoiceChatHandler.OnVoiceLevelChanged -= OnVoiceLevelChangedEvent;

            groupTransform.gameObject.SetActive(false);
        }

        public override void OnStartLocalPlayer()
        {
            base.OnStartLocalPlayer();
            groupTransform.gameObject.SetActive(false);
        }

        private void OnPlayerDisplayNameChangedEvent(Player player, string displayName)
        {
            displayNameUI.SetDisplayName(displayName);
            displayNameUI.Visible = true;
        }

        private void SetDisplayNameStyle(IDisplayNameBgStyle.DisplayNameBgStyle displayNameBgStyle)
        {
            displayNameUI.SetStyle(displayNameBgStyle);
        }

        private void OnPlayerGameModeChangedEvent(Player player, GameMode gameMode)
        {
            if (gameMode != GameMode.SURVIVAL)
            {
                displayNameUI.Visible = false;
            }
        }

        #region VOICE CHAT
        private void OnVoiceLevelChangedEvent(float volume)
        {
            voiceChatUI.SetVolume(volume);
            voiceChatUI.Visible = true;
            
            //Auto Hide VoiceChat Icon
            CancelInvoke(nameof(HideVoiceChat));
            Invoke(nameof(HideVoiceChat),  1f);
        }

        private void HideVoiceChat()
        {
            voiceChatUI.Visible = false;
        }
        
        #endregion

        #region PLAYER NAMETAG COLOR

        public static void SetPlayerNameTagColor(Player player,
            IDisplayNameBgStyle.DisplayNameBgStyle displayNameBgStyle)
        {
            _i.RpcSetPlayerNameTagColor(player.GetIdentity(), displayNameBgStyle);
        }

        [ClientRpc]
        private void RpcSetPlayerNameTagColor(GameObject playerGameObject,
            IDisplayNameBgStyle.DisplayNameBgStyle displayNameBgStyle)
        {
            ClientUIManager client = playerGameObject.GetComponent<ClientUIManager>();
            client.SetDisplayNameStyle(displayNameBgStyle);
        }

        #endregion
    }
}