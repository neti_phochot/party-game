﻿namespace ui.messagebox
{
    public interface IMessageBoxUI
    {
        void Show(string header, string s);
        void Hide();
    }
}