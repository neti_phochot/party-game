﻿using Singleton;
using UnityEngine;

namespace ui.messagebox
{
    public class MessageBoxManager : ResourceSingleton<MessageBoxManager>
    {
        private IMessageBoxUI _messageBoxUI;

        public override void Awake()
        {
            base.Awake();

            _messageBoxUI = transform.GetComponentInChildren<IMessageBoxUI>();
            _messageBoxUI.Hide();
        }

        public void ShowText(string header, string message)
        {
            
            _messageBoxUI.Show(header,message);
        }

#if UNITY_EDITOR
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                _messageBoxUI.Show("Test", "Hello, How are you?");
            }
        }
#endif
    }
}