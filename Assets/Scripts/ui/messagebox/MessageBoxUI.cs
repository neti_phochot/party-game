﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ui.messagebox
{
    public class MessageBoxUI : MonoBehaviour, IMessageBoxUI
    {
        [SerializeField] private TextMeshProUGUI headerText;
        [SerializeField] private TextMeshProUGUI message;
        [SerializeField] private Button closeButton;

        [Header("ANIMATION")] [SerializeField] private float duration;
        [SerializeField] private Ease popupAnime;
        [SerializeField] private Transform boxTransform;

        private void Awake()
        {
            closeButton.onClick.AddListener(Hide);
            Debug.Assert(closeButton != null, $"{nameof(closeButton)} == null");
        }

        public void Show(string header, string s)
        {
            headerText.text = header;
            message.text = s;

            DOTween.Kill(boxTransform);
            Visible = true;
            boxTransform.localScale = Vector3.one * 0.001f;
            boxTransform.DOScale(Vector3.one, duration)
                .SetEase(popupAnime);
        }

        public void Hide()
        {
            DOTween.Kill(boxTransform);
            boxTransform.localScale = Vector3.one;
            Visible = false;
        }


        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}