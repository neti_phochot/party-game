﻿using TMPro;
using UnityEngine;

namespace ui.celebrating
{
    public class CelebratingUI : MonoBehaviour , IVisible
    {
        [SerializeField] private TextMeshProUGUI winnerText;

        public void SetWinner(string winner)
        {
            winnerText.text = winner;
        }
        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}