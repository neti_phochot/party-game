﻿using TMPro;
using UnityEngine;

namespace ui.lobby
{
    public class LobbyPlayerCount : MonoBehaviour, ILobbyPlayerCountUI
    {
        [SerializeField] private TextMeshProUGUI playerCountText;

        public void SetLobbyPlayerCount(int playerCount, int maxPlayers)
        {
            playerCountText.text = $"{playerCount}/{maxPlayers}";
        }

        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}