﻿using TMPro;
using UnityEngine;

namespace ui.lobby
{
    public class LobbyReadyCount : MonoBehaviour, ILobbyReadyCountUI
    {
        [SerializeField] private TextMeshProUGUI readyCountText;

        public void SetLobbyReadyCount(int ready, int minimumPlayer)
        {
            readyCountText.text = $"Ready: {ready}/{minimumPlayer}";
        }

        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}