﻿namespace ui.lobby
{
    public interface ILobbyPlayerCountUI : IVisible
    {
        void SetLobbyPlayerCount(int playerCount, int maxPlayers);
    }
}