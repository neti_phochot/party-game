﻿using networking;
using UnityEngine;
using UnityEngine.UI;

namespace ui.player
{
    public class PauseMenuUI : MonoBehaviour, IVisible
    {
        [SerializeField] private Button disconnectButton;
        [SerializeField] private Button backButton;

        private void Awake()
        {
            disconnectButton.onClick.AddListener(Disconnect);
            backButton.onClick.AddListener(ToggleMenu);
        }

        public void ToggleMenu()
        {
            Visible = !Visible;
        }

        private void Disconnect()
        {
            MyNetworkManager.Instance.LeaveLobby();
        }

        public bool Visible
        {
            get => gameObject.activeSelf;
            set
            {
                Cursor.visible = value;
                Cursor.lockState = value ? CursorLockMode.None : CursorLockMode.Locked;

                gameObject.SetActive(value);
            }
        }
    }
}