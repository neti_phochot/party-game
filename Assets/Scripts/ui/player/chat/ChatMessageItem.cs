﻿using TMPro;
using UnityEngine;

namespace ui.player.chat
{
    public class ChatMessageItem : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI messageText;

        public void SetMessage(string message)
        {
            messageText.text = message;
        }
    }
}