﻿using networking;
using TMPro;
using ui.messagebox;
using UnityEngine;
using UnityEngine.UI;

namespace ui.player
{
    public class InviteCodeUI : MonoBehaviour, IVisible
    {
        [SerializeField] private TextMeshProUGUI servernameText;
        [SerializeField] private TextMeshProUGUI inviteCodeText;
        [SerializeField] private Button copyButton;
        [SerializeField] private Button revealButton;
        [SerializeField] private Transform revealTransform;

        private void Awake()
        {
            copyButton.onClick.AddListener(CopyCode);
            revealButton.onClick.AddListener(RevealCode);
        }

        private void Start()
        {
            servernameText.text = MyNetworkManager.Instance.ServerName;
            inviteCodeText.text = MyNetworkManager.Instance.InviteCode;
        }

        private void RevealCode()
        {
            revealTransform.gameObject.SetActive(!revealTransform.gameObject.activeSelf);
        }

        private void CopyCode()
        {
            GUIUtility.systemCopyBuffer = MyNetworkManager.Instance.InviteCode;
            MessageBoxManager.Instance.ShowText("Invite Code", "Invite code copied to your clipboard!");
        }

        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}