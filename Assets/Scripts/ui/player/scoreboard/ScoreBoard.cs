﻿using System.Linq;
using System.Threading.Tasks;
using core.entity.player;
using game.gameplayer;
using Mirror;
using networking;
using UnityEngine;

namespace ui.player.scoreboard
{
    public class ScoreBoard : NetworkBehaviour, IVisible
    {
        private static bool _isInitScoreboard;

        [Range(1, 10)] [SerializeField] private int scoreboardUpdateDelay = 2;
        [SerializeField] private ScoreBoardItem scoreBoardItemTemplate;
        [SerializeField] private Transform container;
        [SerializeField] private Transform scoreboardTransform;

        private bool _isDestroyed;
        private bool _isUpdating;

        private void Awake()
        {
            scoreBoardItemTemplate.gameObject.SetActive(false);
        }

        private void Start()
        {
            Hide();
        }

        private void OnEnable()
        {
            PlayerInput playerInput = GetComponent<PlayerInput>();
            playerInput.OnInputAction += OnInputActionEvent;
            Show();
        }

        private void OnDisable()
        {
            PlayerInput playerInput = GetComponent<PlayerInput>();
            playerInput.OnInputAction -= OnInputActionEvent;
            Hide();
        }

        private void OnInputActionEvent(PlayerInput.EInputAction inputAction)
        {
            switch (inputAction)
            {
                case PlayerInput.EInputAction.TAB_KEY_DOWN:
                    Show();
                    break;
                case PlayerInput.EInputAction.TAB_KEY_UP:
                    Hide();
                    break;
            }
        }

        public override void OnStartServer()
        {
            if (_isInitScoreboard) return;
            _isInitScoreboard = true;
            base.OnStartServer();
            MyNetworkManager.OnPlayerStatusChanged += OnPlayerStatusChangedEvent;
            MyNetworkManager.OnPlayerJoined += OnPlayerJoinedEvent;
            MyNetworkManager.OnPlayerQuit += OnPlayerQuitEvent;
        }

        public override void OnStopServer()
        {
            if (!_isInitScoreboard) return;
            _isInitScoreboard = false;
            base.OnStopServer();
            MyNetworkManager.OnPlayerStatusChanged -= OnPlayerStatusChangedEvent;
            MyNetworkManager.OnPlayerJoined -= OnPlayerJoinedEvent;
            MyNetworkManager.OnPlayerQuit -= OnPlayerQuitEvent;
        }

        private void OnPlayerStatusChangedEvent(GamePlayer gamePlayer)
        {
            TryUpdateScoreboard();
        }

        private void OnPlayerJoinedEvent(GamePlayer gamePlayer)
        {
            TryUpdateScoreboard();
        }

        private void OnPlayerQuitEvent(GamePlayer gamePlayer)
        {
            TryUpdateScoreboard();
        }

        private void OnDestroy()
        {
            _isDestroyed = true;
        }

        private async void TryUpdateScoreboard()
        {
            if (_isUpdating) return;
            _isUpdating = true;
            await Task.Delay(scoreboardUpdateDelay * 1000);
            if (_isDestroyed) return;
            RpcUpdatePlayerScoreBoard(MyNetworkManager.Instance.GetOnlineGamePlayers().Values.ToArray());
            _isUpdating = false;
        }
        
        [ClientRpc]
        private void RpcUpdatePlayerScoreBoard(GamePlayer[] gamePlayers)
        {
            NetworkClient.localPlayer.gameObject.GetComponent<ScoreBoard>().UpdatePlayerScoreboard(gamePlayers);
        }

        private void UpdatePlayerScoreboard(GamePlayer[] gamePlayers)
        {
            foreach (Transform child in container)
            {
                child.gameObject.SetActive(false);
            }

            for (int i = 0; i < gamePlayers.Length; i++)
            {
                GamePlayer gamePlayer = gamePlayers[i];
                ScoreBoardItem playerScoreBoardItem = i >= container.childCount
                    ? Instantiate(scoreBoardItemTemplate, container)
                    : container.GetChild(i).GetComponent<ScoreBoardItem>();

                playerScoreBoardItem.SetDisplayName(gamePlayer.ToPlayer().GetDisplayName());
                playerScoreBoardItem.SetStatus(gamePlayer.status);
                playerScoreBoardItem.SetPing(100);
                playerScoreBoardItem.gameObject.SetActive(true);
            }
        }

        public bool Visible
        {
            get => scoreboardTransform.gameObject.activeSelf;
            set => scoreboardTransform.gameObject.SetActive(value);
        }

        public void Show()
        {
            scoreboardTransform.gameObject.SetActive(true);
        }

        public void Hide()
        {
            scoreboardTransform.gameObject.SetActive(false);
        }
    }
}