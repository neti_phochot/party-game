﻿using TMPro;
using UnityEngine;

namespace ui.player.scoreboard
{
    public class ScoreBoardItem : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI displayNameText;
        [SerializeField] private TextMeshProUGUI statusText;
        [SerializeField] private TextMeshProUGUI pingText;
        [SerializeField] private Transform deathOverlay;

        public void SetDisplayName(string displayName)
        {
            displayNameText.text = displayName;
        }

        public void SetStatus(string status)
        {
            statusText.text = status;
            deathOverlay.gameObject.SetActive(status.Equals("DEAD"));
        }

        public void SetPing(int ping)
        {
            pingText.text = $"{ping}";
        }
    }
}