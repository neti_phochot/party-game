﻿using System.Threading.Tasks;
using assets;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ui.loadingscreen
{
    public class LoadingScreenUI : MonoBehaviour, ILoadingScreenUI
    {
        [SerializeField] private TextMeshProUGUI mapNameText;
        [SerializeField] private TextMeshProUGUI descriptionText;
        [SerializeField] private Image mapPreviewImage;
        [SerializeField] private Transform infoTransform;
        [Header("FADE")] [SerializeField] private CanvasGroup canvasGroup;

        [Header("LOADING ICON")] [SerializeField]
        private RectTransform loadingIcon;

        [SerializeField] private float duration;
        [Range(0, 1f)] [SerializeField] private float previewDurationScale;

        private void Awake()
        {
            Visible = false;

            SpinIcon();
        }

        private void SpinIcon()
        {
            loadingIcon.DOLocalRotate(new Vector3(0, 0, 180f), duration)
                .SetLoops(-1, LoopType.Restart)
                .SetEase(Ease.Linear);
        }

        public void LoadingScreen(bool isLoadingStart, float loadSceneDelay, MapAssetSO.GameMap gameMap)
        {
            loadSceneDelay *= 0.5f;
            Visible = true;

            bool isEmpty = string.IsNullOrEmpty($"{gameMap.mapName}{gameMap.description}");
            infoTransform.gameObject.SetActive(!isEmpty);

            mapNameText.text = gameMap.mapName;
            descriptionText.text = gameMap.description;
            mapPreviewImage.sprite = gameMap.mapPreview;

            //Fade
            float previewDuration = loadSceneDelay * previewDurationScale;
            float fadeDuration = loadSceneDelay - previewDuration;

            float startValue = isLoadingStart ? 0 : 1;
            canvasGroup.alpha = startValue;

            Color startColor = mapPreviewImage.color;
            startColor.a = startValue;
            mapPreviewImage.color = startColor;

            if (isLoadingStart)
            {
                canvasGroup.DOFade(1, fadeDuration).OnComplete(() => { mapPreviewImage.DOFade(1, fadeDuration); });
            }
            else
            {
                transform.DOScale(Vector3.one, previewDuration)
                    .OnComplete(() =>
                    {
                        mapPreviewImage.DOFade(0, fadeDuration).OnComplete(() =>
                        {
                            canvasGroup.DOFade(0, fadeDuration).OnComplete(() => { Visible = false; });
                        });
                    });
            }
        }

        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}