﻿namespace ui
{
    public interface IVisible
    {
        bool Visible { get; set; }
    }
}