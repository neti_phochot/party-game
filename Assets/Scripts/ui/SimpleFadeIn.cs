﻿using DG.Tweening;
using UnityEngine;

namespace ui
{
    public class SimpleFadeIn : MonoBehaviour
    {
        [SerializeField] private float delay;
        [SerializeField] private float duration;
        [SerializeField] private CanvasGroup canvasGroup;

        private void Start()
        {
            canvasGroup.alpha = 1f;
            Invoke(nameof(DoFade), delay);
        }

        private void DoFade()
        {
            canvasGroup.DOFade(0, duration).OnComplete(() => { Destroy(gameObject); });
        }
    }
}