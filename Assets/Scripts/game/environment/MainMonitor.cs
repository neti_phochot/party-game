﻿using core.entity;
using core.entity.player;
using Mirror;
using networking;
using UnityEngine;

namespace game.environment
{
    public class MainMonitor : NetworkBehaviour
    {
        [SerializeField] private DeathPlayerMonitor[] childMonitors;

        public override void OnStartServer()
        {
            base.OnStartServer();
            Invoke(nameof(SubscriptDelay), 1f);
        }

        private void SubscriptDelay()
        {
            foreach (var player in MyNetworkManager.Instance.GetOnlinePlayers())
            {
                player.OnPlayerDeath += OnPlayerDeathEvent;
            }
        }

        private void OnPlayerDeathEvent(HumanEntity humanEntity, DeathEffect deathEffect)
        {
            Player player = (Player)humanEntity;
            SendAnnounced(player.GetDisplayName());
        }

        [ClientRpc]
        private void SendAnnounced(string victim)
        {
            foreach (DeathPlayerMonitor child in childMonitors)
            {
                child.Announced(victim);
            }
        }
    }
}