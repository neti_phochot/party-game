﻿using core.entity.player;
using Mirror;
using UnityEngine;

namespace game.environment
{
    public class QualifyTrigger : MonoBehaviour
    {
        private BaseGame _game;
        private void Awake()
        {
            _game = FindObjectOfType<BaseGame>();
            if (!_game)
            {
                Destroy(gameObject);
            }
        }

        [Server]
        private void OnTriggerEnter(Collider other)
        {
            if (!other.TryGetComponent(out Player player)) return;
            if (_game.IsQualified(player)) return;
            _game.AddQualifiedPlayer(player);
            
        }

        [Server]
        private void OnTriggerExit(Collider other)
        {
            if (!other.TryGetComponent(out Player player)) return;
            if (!_game.IsQualified(player)) return;
            _game.RemoveQualifiedPlayer(player);
        }
        

    }
}