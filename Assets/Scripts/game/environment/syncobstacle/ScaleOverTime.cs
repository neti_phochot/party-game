﻿using DG.Tweening;
using UnityEngine;

namespace game.environment.syncobstacle
{
    public class ScaleOverTime: BaseSyncObstacle
    {
        [SerializeField] private bool loop = true;
        [SerializeField] private float startDelay;
        [SerializeField] private float duration;
        [SerializeField] private float scaleLength;
        [SerializeField] private Vector3 scaleWorldDirection;
        [SerializeField] private Ease ease = Ease.Linear;

        protected override void Sync()
        {
            Invoke(nameof(DoMove), startDelay);
        }

        private void DoMove()
        {
            if (loop)
            {
                transform
                    .DOScale(transform.localScale + scaleWorldDirection * scaleLength, duration)
                    .SetEase(ease)
                    .SetLoops(-1, LoopType.Yoyo);
            }
            else
            {
                transform
                    .DOScale(transform.localScale + scaleWorldDirection * scaleLength, duration)
                    .SetEase(ease);
            }
        }
    }
}