﻿using System.Collections.Generic;
using System.Linq;
using core.entity;
using core.entity.player;
using Mirror;
using UnityEngine;

namespace game.environment
{
    public class DeathObstacle : NetworkBehaviour
    {
        [SerializeField] private bool instanceKill;

        [Header("DAMAGE OVER TIME")] [Range(1, 10)] [SerializeField]
        private int damageInterval = 1;

        [Range(0, 1f)] [SerializeField] private float damageScale;

        private readonly List<Player> _victims = new List<Player>();

        private void OnEnable()
        {
            InvokeRepeating(nameof(DamageLoop), 0, damageInterval);
        }

        private void OnDisable()
        {
            CancelInvoke(nameof(DamageLoop));
        }

        private void DamageLoop()
        {
            foreach (var victim in _victims.Where(victim => victim).Where(victim => !victim.IsDead()))
            {
                victim.Damage(victim.GetMaxHealth() * damageScale, DeathEffect.RAGDOLL);
            }
        }

        private void Kill(Player player)
        {
            player.Damage(player.GetMaxHealth(), DeathEffect.EXPLODE);
        }

        private void AddVictim(Player player)
        {
            if (_victims.Contains(player)) return;

            if (instanceKill)
            {
                Kill(player);
                return;
            }

            _victims.Add(player);
        }

        private void RemoveVictim(Player player)
        {
            if (!_victims.Contains(player)) return;
            _victims.Remove(player);
        }

        #region TRIGGER

        [ServerCallback]
        private void OnTriggerEnter(Collider other)
        {
            if (!other.TryGetComponent<Player>(out var player)) return;
            AddVictim(player);
        }

        [ServerCallback]
        private void OnTriggerExit(Collider other)
        {
            if (!other.TryGetComponent<Player>(out var player)) return;
            if (instanceKill) return;
            RemoveVictim(player);
        }

        #endregion


        #region COLLISION

        [ServerCallback]
        private void OnCollisionEnter(Collision collision)
        {
            if (!collision.collider.TryGetComponent<Player>(out var player)) return;
            if (instanceKill)
            {
                Kill(player);
                return;
            }

            AddVictim(player);
        }

        #endregion

        [ServerCallback]
        private void OnCollisionExit(Collision other)
        {
            if (!other.collider.TryGetComponent<Player>(out var player)) return;
            if (instanceKill) return;
            RemoveVictim(player);
        }
    }
}