﻿using core.entity.player;
using UnityEngine;

namespace game.environment
{
    public class Ladder : MonoBehaviour
    {
        [Range(0.1f, 1f)] [SerializeField] private float sensitivity;
        [SerializeField] private string playerTag;
        [SerializeField] private float upSpeed;
        [SerializeField] private float downSpeed;
        [SerializeField] private float staySpeed;
        [SerializeField] private float cancelForce;
        [Range(0.1f, 1f)] [SerializeField] private float xzSpeedFactor;

        private PlayerMovement _playerMovement;

        private bool _isRequestUp;
        private bool _isRequestDown;
        private bool _isRequestCancel;

        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag(playerTag)) return;
            _playerMovement = other.GetComponent<PlayerMovement>();

            PlayerInput playerInput = other.GetComponent<PlayerInput>();
            playerInput.OnInputValueUpdated += OnInputValueUpdatedEvent;
            playerInput.OnInputAction += OnInputActionEvent;
        }

        private void FixedUpdate()
        {
            Climb();
        }

        private void OnInputActionEvent(PlayerInput.EInputAction inputAction)
        {
            switch (inputAction)
            {
                case PlayerInput.EInputAction.JUMP_KEY_DOWN:
                    _isRequestCancel = true;
                    break;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (!other.CompareTag(playerTag)) return;

            _playerMovement = null;
            _isRequestCancel = false;
            _isRequestUp = false;
            _isRequestDown = false;

            PlayerInput playerInput = other.GetComponent<PlayerInput>();
            playerInput.OnInputValueUpdated -= OnInputValueUpdatedEvent;
            playerInput.OnInputAction -= OnInputActionEvent;
        }

        private void OnInputValueUpdatedEvent(PlayerInput.EInputValue inputType, float value)
        {
            switch (inputType)
            {
                case PlayerInput.EInputValue.VERTICAL:
                    _isRequestUp = value >= sensitivity;
                    _isRequestDown = value <= -sensitivity;
                    break;
            }
        }

        private void Climb()
        {
            if (!_playerMovement || _playerMovement.isKnockOut) return;
            if (_isRequestCancel)
            {
                _playerMovement.rb.velocity = Vector3.zero;
                _playerMovement.rb.AddForce(-transform.forward * cancelForce, ForceMode.Impulse);
                _playerMovement = null;
                return;
            }

            Vector3 velocity = _playerMovement.rb.velocity;
            velocity *= xzSpeedFactor;

            if (_isRequestUp)
            {
                velocity.y = upSpeed;
            }
            else if (_isRequestDown)
            {
                velocity.y = -downSpeed;
            }
            else
            {
                velocity.y = staySpeed;
            }

            velocity += transform.forward;
            _playerMovement.rb.velocity = velocity;
        }
    }
}