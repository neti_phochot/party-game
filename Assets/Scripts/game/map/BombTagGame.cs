﻿using System.Collections.Generic;
using System.Linq;
using core.entity;
using core.entity.player;
using core.interact;
using scriptsableobject;
using sfx;
using ui.client;
using ui.game;
using UnityEngine;
using utils;
using Material = core.Material;

namespace game.map
{
    public class BombTagGame : BaseGame
    {
        [Header("SFX")] [SerializeField] private EnumValue tagSFX;
        [Header("GAME")] [SerializeField] private int explodeDelay = 10;

        private readonly List<Player> _explodedPlayer = new List<Player>();
        private int _bombTimer;

        #region GAME LOGIC

        protected override void OnPlayerJoinedEvent(Player player)
        {
            UpdateTeamCount();
        }

        protected override void OnGameStartEvent()
        {
            SetRandomTagPlayer();
            RestartBombTimer();
        }

        protected override void OnPlayerQuitEvent(Player player)
        {
            PlayerLoss(player);
            UpdateTeamCount();
        }

        protected override void OnPlayerDeathEvent(Player player)
        {
            PlayerLoss(player);
            UpdateTeamCount();
        }

        protected override void OnTimeOutEvent()
        {
            ForceQualifyPlayers(GetAlivePlayers());
        }

        protected override void OnGameOverEvent()
        {
            StopBombTimer();
        }

        private void PlayerLoss(Player player)
        {
            if (CheckWinners()) return;
            if (_explodedPlayer.Contains(player)) return;
            if (!player.HasItem(Material.BOMB)) return;
            SetRandomTagPlayer();
        }

        #endregion

        #region GAME FUNCTION

        private bool CheckWinners()
        {
            Player lastOneStanding = GetLastOneStandingPlayer();
            if (!lastOneStanding) return false;
            ForceQualifyPlayers(new List<Player> { lastOneStanding });
            return true;
        }

        private void SetRandomTagPlayer()
        {
            if (IsGameOver) return;
            Player tagPlayer = GetRandomPlayerWithItem(Material.BOMB, false);
            if (!tagPlayer) return;
            SetBomberPlayer(tagPlayer);
        }

        private void SetBomberPlayer(Player player)
        {
            player.SetHeldItemSlot(0);
            player.SetItemInHand(Material.BOMB);

            UpdateTeamCount();
            ClientUIManager.SetPlayerNameTagColor(player, IDisplayNameBgStyle.DisplayNameBgStyle.RED);
            SoundInstance.Instance.PlaySound(tagSFX, player.transform.position);
        }

        private void UnSetBomberPlayer(Player player)
        {
            for (int i = 0; i < player.GetContents().Length; i++)
            {
                if (player.GetItem(i).material != Material.BOMB) continue;
                player.SetHeldItemSlot(i);
                player.SetItemInHand(Material.AIR);
            }

            UpdateTeamCount();
            ClientUIManager.SetPlayerNameTagColor(player, IDisplayNameBgStyle.DisplayNameBgStyle.BLUE);
        }

        private void RestartBombTimer()
        {
            StopBombTimer();
            Invoke(nameof(OnBombExplode), explodeDelay);
        }

        private void StopBombTimer()
        {
            CancelInvoke(nameof(OnBombExplode));
        }

        private void OnBombExplode()
        {
            foreach (var player in GetAlivePlayers())
            {
                if (!player.HasItem(Material.BOMB)) continue;
                _explodedPlayer.Add(player);
                player.Damage(player.GetMaxHealth(), DeathEffect.EXPLODE);
                SetRandomTagPlayer();
            }

            RestartBombTimer();
        }

        private (int good, int bad) GetTeamPlayerCount()
        {
            List<Player> players = GetAlivePlayers();
            int bad = players.Count(p => p.HasItem(Material.BOMB));
            int good = players.Count - bad;
            return (good, bad);
        }

        #endregion

        #region UI

        private void UpdateTeamCount()
        {
            (int good, int bad) teamCount = GetTeamPlayerCount();
            UI.team = new GameUIManager.TeamPlayerData
            {
                good = teamCount.good,
                bad = teamCount.bad,
            };
        }

        #endregion

        //--------------------------
        // Event handler
        //--------------------------
        protected override void OnPlayerUseItemEvent(Player p, ClickActionType clickActionType)
        {
            if (clickActionType != ClickActionType.DOWN) return;

            if (p.GetItemInHand().material == Material.BOMB)
            {
                p.SetTargetAction(1, (other) =>
                {
                    Vector3 pushDir = (other.GetLocation() - p.GetLocation()).normalized;
                    other.AddVelocity(pushDir * 15f);

                    SetBomberPlayer(other);
                    UnSetBomberPlayer(p);
                });
            }
        }
    }
}