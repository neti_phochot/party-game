﻿using System.Collections.Generic;
using System.Linq;
using core.entity;
using core.entity.player;
using core.interact;
using ui.client;
using ui.game;
using UnityEngine;
using utils;
using Material = core.Material;

namespace game.map
{
    public class HideAndSeekGame : BaseGame
    {
        #region GAME LOGIC

        protected override void OnPlayerJoinedEvent(Player player)
        {
            ClientUIManager.SetPlayerNameTagColor(player, IDisplayNameBgStyle.DisplayNameBgStyle.YELLOW);
            UpdateTeamCount();
        }

        protected override void OnGameStartEvent()
        {
            int alive = GetAlivePlayers().Count;
            int seekerAmount = 1;

            if (alive <= 2)
            {
                seekerAmount = 1;
            }
            else if (alive <= 10)
            {
                seekerAmount = 2;
            }
            else if (alive <= 20)
            {
                seekerAmount = 3;
            }
            else
            {
                seekerAmount = 4;
            }

            for (int i = 0; i < seekerAmount; i++)
            {
                GiveKnifeRandomPlayer();
            }
        }

        protected override void OnPlayerQuitEvent(Player player)
        {
            if (IsSeeker(player))
            {
                GiveKnifeRandomPlayer();
            }

            UpdateTeamCount();
        }

        protected override void OnPlayerDeathEvent(Player player)
        {
            PlayerLoss();
            UpdateTeamCount();
        }

        protected override void OnTimeOutEvent()
        {
            KillSeeker();
            ForceQualifyPlayers(GetAlivePlayers());
        }

        private void PlayerLoss()
        {
            if (IsGameOver) return;
            Player[] goodTeam = GetTeamPlayers(false);
            Player[] seekerTeam = GetTeamPlayers(true);

            //Seeker win
            if (goodTeam.Length == 0)
            {
                ForceQualifyPlayers(new List<Player>(seekerTeam));
                return;
            }

            //Hider win
            if (seekerTeam.Length == 0)
            {
                ForceQualifyPlayers(new List<Player>(goodTeam));
                return;
            }
        }

        #endregion

        #region GAME FUNCTION

        private void KillSeeker()
        {
            foreach (Player player in GetAlivePlayers().Where(IsSeeker))
            {
                player.Damage(player.GetMaxHealth(), DeathEffect.EXPLODE);
            }
        }

        private void GiveKnifeRandomPlayer()
        {
            Player seeker = GetRandomPlayerWithItem(Material.KNIFE, false);
            if (!seeker) return;
            AddSeeker(seeker);
        }

        private void AddSeeker(Player player)
        {
            player.SetHeldItemSlot(0);
            player.SetItemInHand(Material.KNIFE);

            UpdateTeamNameTage();
            UpdateTeamCount();
        }

        private void RemoveSeeker(Player player)
        {
            for (int i = 0; i < player.GetContents().Length; i++)
            {
                if (player.GetItem(i).material != Material.KNIFE) continue;
                player.SetHeldItemSlot(i);
                player.SetItemInHand(Material.AIR);
            }

            UpdateTeamNameTage();
            UpdateTeamCount();
        }

        private bool IsSeeker(Player player)
        {
            return player.HasItem(Material.KNIFE);
        }

        private (int good, int bad) GetTeamPlayerCount()
        {
            List<Player> players = GetAlivePlayers();
            int bad = players.Count(p => p.HasItem(Material.KNIFE));
            int good = players.Count(p => !p.HasItem(Material.KNIFE));
            return (good, bad);
        }

        private Player[] GetTeamPlayers(bool seekerTeam)
        {
            return GetAlivePlayers().Where(p => IsSeeker(p) == seekerTeam).ToArray();
        }

        #endregion

        #region UI

        private void UpdateTeamCount()
        {
            (int good, int bad) teamCount = GetTeamPlayerCount();
            UI.team = new GameUIManager.TeamPlayerData
            {
                good = teamCount.good,
                bad = teamCount.bad,
            };
        }

        private void UpdateTeamNameTage()
        {
            foreach (var player in GetAlivePlayers())
            {
                ClientUIManager.SetPlayerNameTagColor(player,
                    IsSeeker(player)
                        ? IDisplayNameBgStyle.DisplayNameBgStyle.RED
                        : IDisplayNameBgStyle.DisplayNameBgStyle.YELLOW);
            }
        }

        #endregion

        protected override void OnPlayerUseItemEvent(Player p, ClickActionType clickActionType)
        {
            if (clickActionType != ClickActionType.DOWN) return;

            if (p.GetItemInHand().material == Material.KNIFE)
            {
                p.SetTargetAction(1, (other) =>
                {
                    if (IsSeeker(other)) return;

                    Vector3 pushDir = (other.GetLocation() - p.GetLocation()).normalized;
                    other.AddVelocity(pushDir * 5f);
                    other.Damage(50f, DeathEffect.RAGDOLL);

                    if (other.IsDead())
                    {
                        RemoveSeeker(p);
                    }
                });
            }
        }
    }
}