﻿using System.Collections.Generic;
using System.Linq;
using core.entity.player;
using core.interact;
using core.inventory.item;
using ui.client;
using ui.game;
using UnityEngine;
using utils;
using Material = core.Material;

namespace game.map
{
    public class MyBananaGame : BaseGame
    {
        [Header("MY BANANA GAME")] [SerializeField]
        private bool giveScoreOnHoldOnly = true;

        [SerializeField] private float scoreBananaPlayerDelay = 0.5f;

        [Header("SCORE COLOR")] [SerializeField]
        private Color qualifiedScoreColor = Color.white;

        [SerializeField] private Color notQualifiedScoreColor = Color.red;

        private readonly Dictionary<Player, int> _playerState = new Dictionary<Player, int>();

        #region GAME LOGIC

        protected override void OnPlayerJoinedEvent(Player player)
        {
            UpdatePlayerCount();
            AddPlayerScore(player, 0);
        }

        protected override void OnGameStartEvent()
        {
            StartAddBananaPlayerScore();

            int alive = GetAlivePlayers().Count;
            int seekerAmount = 1;

            if (alive <= 3)
            {
                seekerAmount = 1;
            }
            else if (alive <= 5)
            {
                seekerAmount = 2;
            }
            else if (alive <= 10)
            {
                seekerAmount = 4;
            }
            else if (alive <= 20)
            {
                seekerAmount = 7;
            }
            else
            {
                seekerAmount = 10;
            }

            for (int i = 0; i < seekerAmount; i++)
            {
                SetRandomBananaPlayer();
            }
        }

        protected override void OnPlayerDeathEvent(Player player)
        {
            PlayerLoss(player);
            UpdatePlayerCount();
        }

        protected override void OnPlayerQuitEvent(Player player)
        {
            PlayerLoss(player);
            UpdatePlayerCount();
        }

        protected override void OnTimeOutEvent()
        {
            ForceQualifyPlayers(GetQualifiedBananaPlayers());
        }

        protected override void OnGameOverEvent()
        {
            StopAddBananaPlayerScore();
        }

        private void PlayerLoss(Player player)
        {
            RemovePlayerScore(player);

            if (!IsBananaPlayer(player)) return;
            SetRandomBananaPlayer();
        }

        #endregion

        #region GAME FUNCTION

        private bool IsAllBananaPlayers()
        {
            return GetPlayersWithItem(Material.BANANA, false).Length == 0;
        }

        private bool IsBananaPlayer(Player player)
        {
            return player.HasItem(Material.BANANA);
        }

        private void SetRandomBananaPlayer()
        {
            Player bananaPlayer = GetRandomPlayerWithItem(Material.BANANA, false);
            if (!bananaPlayer) return;
            SetBananaPlayer(bananaPlayer);

            if (IsAllBananaPlayers())
            {
                ForceQualifyPlayers(GetQualifiedBananaPlayers());
            }
        }

        private void SetBananaPlayer(Player bananaPlayer)
        {
            bananaPlayer.SetHeldItemSlot(0);
            bananaPlayer.SetItemInHand(Material.BANANA);
            ClientUIManager.SetPlayerNameTagColor(bananaPlayer, IDisplayNameBgStyle.DisplayNameBgStyle.YELLOW);
        }

        private void UnsetBananaPlayer(Player player)
        {
            for (int i = 0; i < player.GetContents().Length; i++)
            {
                ItemStack item = player.GetContents()[i];
                if (item.material != Material.BANANA) continue;
                player.SetHeldItemSlot(i);
                player.SetItemInHand(ItemStack.CreateItem(Material.AIR));
                ClientUIManager.SetPlayerNameTagColor(player, IDisplayNameBgStyle.DisplayNameBgStyle.BLUE);
                break;
            }
        }

        private List<Player> GetQualifiedBananaPlayers()
        {
            int qualifiedScore = GetQualifiedScore().qualifiedScore;
            return (from data in GetSortScorePlayers()
                where _playerState.ContainsKey(data.Key)
                where data.Value >= qualifiedScore
                select data.Key).ToList();
        }

        private bool HasCrown(Player player)
        {
            return player.GetHelmet().material == Material.CROWN;
        }

        #endregion

        #region PLAYER SCORE

        private void StartAddBananaPlayerScore()
        {
            InvokeRepeating(nameof(AddScoreBananaPlayers), 0, scoreBananaPlayerDelay);
        }

        private void StopAddBananaPlayerScore()
        {
            CancelInvoke(nameof(AddScoreBananaPlayers));
        }

        private KeyValuePair<Player, int>[] GetSortScorePlayers()
        {
            return (from dict in _playerState
                orderby dict.Value descending
                select dict).ToArray();
        }

        private (Player topPlayer, int topScore, int qualifiedScore) GetQualifiedScore()
        {
            var sortedScorePlayers = GetSortScorePlayers();
            int count = 0;
            int maxSortedScoreLength = Mathf.Clamp(GetQualifyCount(), 1, sortedScorePlayers.Length);
            foreach (var data in sortedScorePlayers)
            {
                if (++count < maxSortedScoreLength) continue;
                return (sortedScorePlayers[0].Key, sortedScorePlayers[0].Value, data.Value);
            }

            return (null, 0, 0);
        }

        private void AddPlayerScore(Player player, int score)
        {
            if (_playerState.ContainsKey(player)) _playerState[player] += score;
            else _playerState.Add(player, score);
        }

        private int GetPlayerScore(Player player)
        {
            return _playerState.ContainsKey(player) ? _playerState[player] : 0;
        }

        private void AddScoreBananaPlayers()
        {
            (Player topPlayer, int topScore, int qualifiedScore) = GetQualifiedScore();
            foreach (var player in GetAlivePlayers())
            {
                if (HasCrown(player))
                {
                    if (player != topPlayer)
                    {
                        player.SetHelmet(Material.AIR);
                    }
                }
                else if (player == topPlayer)
                {
                    player.SetHelmet(Material.CROWN);
                }

                if (IsBananaPlayer(player))
                {
                    if (player.GetItemInHand().material == Material.BANANA && giveScoreOnHoldOnly)
                    {
                        AddPlayerScore(player, 1);
                    }
                }

                int score = GetPlayerScore(player);
                bool isPlayerScoreQualified = score >= qualifiedScore;
                Color scoreColor = isPlayerScoreQualified ? qualifiedScoreColor : notQualifiedScoreColor;
                UpdatePlayerScore(player, score, scoreColor);
            }
        }

        private void RemovePlayerScore(Player player)
        {
            if (!_playerState.ContainsKey(player)) return;
            _playerState.Remove(player);
        }

        private void UpdatePlayerScore(Player player, int score, Color color)
        {
            //Add score
            UI.AddPlayerScore(player.GetIdentity(), new GameUIManager.PlayerScoreData()
            {
                header = "Your score",
                score = score,
                color = color,
            });
        }

        #endregion

        #region UI

        private void UpdatePlayerCount()
        {
            UI.alive = GetAlivePlayers().Count;
        }

        #endregion

        //--------------------------
        // Event handler
        //--------------------------
        protected override void OnPlayerUseItemEvent(Player p, ClickActionType clickActionType)
        {
            if (clickActionType != ClickActionType.DOWN) return;

            if (p.GetItemInHand().material == Material.AIR)
            {
                p.SetTargetAction(1, (other) =>
                {
                    if (!other.HasItem(Material.BANANA)) return;
                    UnsetBananaPlayer(other);
                    SetBananaPlayer(p);
                });
            }
        }
    }
}