﻿using System;
using System.Collections.Generic;
using System.Linq;
using core.entity;
using core.entity.player;
using core.entity.player.gamemode;
using core.interact;
using game.gameplayer;
using Mirror;
using networking;
using ui.game;
using UnityEngine;
using Material = core.Material;
using Random = UnityEngine.Random;

namespace game
{
    public abstract class BaseGame : NetworkBehaviour
    {
        private const int GAME_START_DELAY = 5;
        private const int GAME_OVER_DELAY = 5;

        [Header("GAME TIMER")] [SerializeField]
        private int roundDuration = 300;

        [Header("QUALIFY")] [Range(0, 1f)] [SerializeField]
        private float qualifyFactor;

        private int _timer;
        private List<Player> _winners = new List<Player>();
        private Action _timerActon;

        private readonly List<Player> _qualifiedPlayers = new List<Player>();
        private int _startsPlayerCount;

        private GameUIManager _gameUI;

        protected GameUIManager UI
        {
            get
            {
                if (!_gameUI)
                {
                    _gameUI = FindObjectOfType<GameUIManager>();
                }

                return _gameUI;
            }
        }

        #region INIT

        public override void OnStartServer()
        {
            base.OnStartServer();
            MyNetworkManager.OnPlayerJoined += OnPlayerJoinedEvent;
            MyNetworkManager.OnPlayerQuit += OnPlayerQuitEvent;

            StartTimer(GAME_START_DELAY, StartGame);
        }

        public override void OnStopServer()
        {
            base.OnStopServer();
            MyNetworkManager.OnPlayerJoined -= OnPlayerJoinedEvent;
            MyNetworkManager.OnPlayerQuit -= OnPlayerQuitEvent;
        }

        #endregion

        #region TIMER

        private void StartTimer(int time, Action action = null)
        {
            _timer = time;
            _timerActon = action;

            StopTimer();
            InvokeRepeating(nameof(OnTimerTick), 0, 1);
        }

        private void StopTimer()
        {
            CancelInvoke(nameof(OnTimerTick));
        }

        private void OnTimerTick()
        {
            UI.timer = _timer;
            if (--_timer > 0) return;
            StopTimer();
            _timerActon?.Invoke();
        }

        #endregion

        #region EVENT

        private void AddUseItemListener()
        {
            foreach (var player in GetAlivePlayers())
            {
                player.GetComponent<PlayerInteract>().OnPlayerUseItem += OnPlayerUseItemEvent;
            }
        }

        private void OnPlayerJoinedEvent(GamePlayer gamePlayer)
        {
            UpdatePlayerCount(true);

            Player player = gamePlayer.ToPlayer();

            if (MyNetworkManager.Instance.GetGameState() != MyNetworkManager.GameState.PREGAME ||
                !MyNetworkManager.Instance.IsQualifiedPlayer(player))
            {
                player.SetGameMode(GameMode.SPECTATOR);
                MyNetworkManager.Instance.SetGamePlayerStatus(player.GetConnectionId(), "SPECTATOR");
                return;
            }

            player.OnPlayerDeath += OnPlayerDeathEvent;
            FreezePlayer(player, true);
            OnPlayerJoinedEvent(player);
        }

        private void OnPlayerQuitEvent(GamePlayer gamePlayer)
        {
            UpdatePlayerCount(false);

            Player player = gamePlayer.ToPlayer();
            player.OnPlayerDeath -= OnPlayerDeathEvent;

            OnPlayerQuitEvent(player);
            CheckGameOver();
        }

        private void UpdatePlayerCount(bool add)
        {
            if (MyNetworkManager.Instance.GetGameState() != MyNetworkManager.GameState.PREGAME) return;
            if (add) _startsPlayerCount++;
            else _startsPlayerCount--;
            Debug.Log($"QUALIFY COUNT: {GetQualifyCount()}/{_startsPlayerCount}");
        }

        private void OnPlayerDeathEvent(HumanEntity humanEntity, DeathEffect deathEffect)
        {
            Player player = (Player)humanEntity;
            MyNetworkManager.Instance.SetGamePlayerStatus(player.GetConnectionId(), "DEAD");

            OnPlayerDeathEvent(player);
            CheckGameOver();
        }

        #endregion

        #region QUALIFY

        public bool IsQualified(Player player)
        {
            return _qualifiedPlayers.Contains(player);
        }

        /// <summary>
        /// Force players to qualify and end the game
        /// </summary>
        /// <param name="players"></param>
        protected void ForceQualifyPlayers(List<Player> players)
        {
            _startsPlayerCount = players.Count;
            qualifyFactor = 1f;
            players.ForEach(AddQualifiedPlayer);
        }

        public void AddQualifiedPlayer(Player player)
        {
            _qualifiedPlayers.Add(player);
            CheckGameOver();
            OnQualifyPlayerChanged();
        }

        public void RemoveQualifiedPlayer(Player player)
        {
            _qualifiedPlayers.Remove(player);
            OnQualifyPlayerChanged();
        }

        protected int GetQualifyCount()
        {
            int qualify = Math.Clamp(Mathf.RoundToInt(_startsPlayerCount * qualifyFactor), 1, int.MaxValue);
            qualify = Mathf.Clamp(qualify, 1, GetAlivePlayers().Count);
            return qualify;
        }

        protected int GetQualifiedCount()
        {
            return _qualifiedPlayers.Count;
        }

        protected bool IsQualifiedReached()
        {
            return GetQualifiedCount() == GetQualifyCount();
        }

        private void KillNoneQualifiedPlayers()
        {
            foreach (var player in GetAlivePlayers().Where(player => !IsQualified(player)))
            {
                Kill(player);
            }
        }

        #endregion

        #region GAME

        private void StartGame()
        {
            MyNetworkManager.Instance.SetGameState(MyNetworkManager.GameState.INGAME);
            FreezePlayers(false);
            AddUseItemListener();
            StartTimer(roundDuration, OnTimeOutEvent);

            OnGameStartEvent();
        }


        private void CheckGameOver()
        {
            if (GetAlivePlayers().Count > 0)
            {
                if (!IsQualifiedReached()) return;
                KillNoneQualifiedPlayers();
            }

            SetWinners(GetAlivePlayers());
        }

        private void SetWinners(List<Player> winners)
        {
            if (IsGameOver) return;
            MyNetworkManager.Instance.SetGameState(MyNetworkManager.GameState.ENDING);
            OnGameOverEvent();

            //Set winner
            _winners = winners;
            UI.playerLeft = winners.Count;

            //Game over delay
            StartTimer(GAME_OVER_DELAY);
            Invoke(nameof(CheckFinalGameResult), GAME_OVER_DELAY);
        }

        [Server]
        private void CheckFinalGameResult()
        {
            //Check if winner still alive
            Player[] players = _winners.Where(p => p && !p.IsDead()).ToArray();

            MyNetworkManager.Instance.ClearQualifiedPlayer();

            if (players.Length == 0)
            {
                Debug.LogWarning("NO ONE WIN THE GAME!");
                MyNetworkManager.Instance.LoadLobby();
                return;
            }

            //Last one standing player
            if (players.Length == 1)
            {
                Player lastPlayer = _winners[0];
                Debug.Log($"{lastPlayer.GetDisplayName()} IS THE WINNER!");
                MyNetworkManager.Instance.LoadWinner(lastPlayer.GetDisplayName());
                return;
            }

            //More than one winner
            MyNetworkManager.Instance.SetQualifyPlayer(new List<Player>(players));
            Debug.Log("----------------");
            foreach (var p in players)
            {
                Debug.Log($"{p.GetDisplayName()} QUALIFIED!");
            }

            Debug.Log("----------------");

            MyNetworkManager.Instance.LoadRandomMap();
        }

        #endregion

        #region GAME EVENT

        protected virtual void OnPlayerJoinedEvent(Player player)
        {
        }

        protected virtual void OnPlayerQuitEvent(Player player)
        {
        }

        protected virtual void OnPlayerDeathEvent(Player player)
        {
        }

        protected virtual void OnPlayerUseItemEvent(Player p, ClickActionType clickActionType)
        {
        }

        protected virtual void OnQualifyPlayerChanged()
        {
        }

        protected virtual void OnGameStartEvent()
        {
        }

        protected virtual void OnGameOverEvent()
        {
        }

        /// <summary>
        /// Kill all alive players by default on time out
        /// </summary>
        protected virtual void OnTimeOutEvent()
        {
            KillPlayers();
        }

        #endregion

        #region GAME FUNTION

        protected bool IsGameOver => MyNetworkManager.Instance.GetGameState() == MyNetworkManager.GameState.ENDING;

        /// <summary>
        /// Get all players on server
        /// include dead, alive players
        /// </summary>
        /// <returns>List of all players</returns>
        protected List<Player> GetAllPlayers()
        {
            return new List<Player>(MyNetworkManager.Instance.GetOnlinePlayers());
        }

        /// <summary>
        /// Get alive players on server
        /// </summary>
        /// <returns>List of alive players</returns>
        protected List<Player> GetAlivePlayers()
        {
            return new List<Player>(MyNetworkManager.Instance.GetOnlinePlayers()
                .Where(p => !p.IsDead() && p.GetGameMode() == GameMode.SURVIVAL));
        }

        /// <summary>
        /// Get last one standing player
        /// </summary>
        /// <returns>winner or null</returns>
        protected Player GetLastOneStandingPlayer()
        {
            List<Player> players = GetAlivePlayers();
            return players.Count == 1 ? players[0] : null;
        }

        /// <summary>
        /// Freeze, UnFreeze player
        /// </summary>
        /// <param name="player"></param>
        /// <param name="isFreeze"></param>
        private void FreezePlayer(Player player, bool isFreeze)
        {
            player.SetWalkSpeed(isFreeze ? 0 : 1);
        }

        /// <summary>
        /// Freeze, UnFreeze alive players
        /// from moving
        /// </summary>
        /// <param name="isFreeze"></param>
        protected void FreezePlayers(bool isFreeze)
        {
            GetAlivePlayers().ForEach(p => FreezePlayer(p, isFreeze));
        }

        /// <summary>
        /// Kill player
        /// </summary>
        /// <param name="player"></param>
        protected void Kill(Player player)
        {
            player.Damage(player.GetMaxHealth(), DeathEffect.EXPLODE);
        }

        /// <summary>
        /// Kill alive players
        /// </summary>
        protected void KillPlayers()
        {
            GetAlivePlayers().ForEach(Kill);
        }

        #endregion

        #region USEFUL

        protected Player GetRandomPlayer()
        {
            List<Player> alivePlayers = GetAlivePlayers();
            return alivePlayers.Count == 0 ? null : alivePlayers[Random.Range(0, alivePlayers.Count)];
        }

        protected Player[] GetPlayersWithItem(Material material, bool hasItem = true)
        {
            return GetAlivePlayers().Where(p => p.HasItem(material) == hasItem).ToArray();
        }

        protected Player GetRandomPlayerWithItem(Material material, bool hasItem = true)
        {
            var noItemPlayers = GetPlayersWithItem(material, hasItem);
            if (noItemPlayers.Length == 0) return null;
            return noItemPlayers[Random.Range(0, noItemPlayers.Length)];
        }

        #endregion

        #region UI

        protected void UpdateQualifyBoard()
        {
            UI.info = new GameUIManager.GameInfoData
            {
                header = "Qualified",
                info = $"{GetQualifiedCount()}/{GetQualifyCount()}"
            };
        }

        #endregion

        #region TEST

#if UNITY_EDITOR
        [ServerCallback]
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.I))
            {
                List<Player> players = GetAlivePlayers();
                List<Player> winners = new List<Player>();

                if (players.Count < 3) return;
                for (int i = 0; i < players.Count; i++)
                {
                    if (i > 1) continue;
                    Player player = players[i];
                    winners.Add(player);
                }

                ForceQualifyPlayers(winners);
            }

            if (Input.GetKeyDown(KeyCode.O))
            {
                ForceQualifyPlayers(GetAlivePlayers());
            }

            if (Input.GetKeyDown(KeyCode.P))
            {
                List<Player> players = GetAlivePlayers();
                List<Player> winners = new List<Player>();

                if (players.Count < 3) return;
                for (int i = 0; i < players.Count; i++)
                {
                    if (i == 0) continue;
                    Player player = players[i];
                    winners.Add(player);
                }

                ForceQualifyPlayers(winners);
            }
        }
#endif

        #endregion
    }
}