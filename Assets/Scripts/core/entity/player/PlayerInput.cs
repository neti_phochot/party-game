﻿using System;
using Mirror;
using UnityEngine;

namespace core.entity.player
{
    public class PlayerInput : NetworkBehaviour
    {
        [SerializeField] private bool lockMouse;

        public enum EInputAction
        {
            LEFT_CLICK_KEY_DOWN,
            LEFT_CLICK_KEY_UP,

            RIGHT_CLICK_KEY_DOWN,
            RIGHT_CLICK_KEY_UP,

            JUMP_KEY_DOWN,
            JUMP_KEY_UP,

            CTRL_KEY_DOWN,
            CTRL_KEY_UP,

            SHIFT_KEY_DOWN,
            SHIFT_KEY_UP,

            C_KEY_DOWN,
            C_KEY_UP,

            E_KEY_DOWN,
            E_KEY_UP,

            TAB_KEY_DOWN,
            TAB_KEY_UP,

            ENTER_KEY_DOWN,
            ENTER_KEY_UP,

            V_KEY_DOWN,
            V_KEY_UP,

            ESC_KEY_DOWN,
            ESC_KEY_UP,
        }

        public enum EInputValue
        {
            VERTICAL,
            HORIZONTAL,
        }

        public enum EMouseValue
        {
            PITCH,
            YAW,
        }

        //Event
        public event Action<EInputAction> OnInputAction;
        public event Action<EInputValue, float> OnInputValueUpdated;
        public event Action<EMouseValue, float> OnMouseValueUpdated;

        private void Awake()
        {
            if (lockMouse)
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.Locked;
            }
        }

        private void Update()
        {
            MyInput();
        }

        private void MyInput()
        {
            if (Input.GetMouseButtonDown(0))
            {
                OnInputAction?.Invoke(EInputAction.LEFT_CLICK_KEY_DOWN);
            }
            else if (Input.GetMouseButtonUp(0))
            {
                OnInputAction?.Invoke(EInputAction.LEFT_CLICK_KEY_UP);
            }

            if (Input.GetMouseButtonDown(1))
            {
                OnInputAction?.Invoke(EInputAction.RIGHT_CLICK_KEY_DOWN);
            }
            else if (Input.GetMouseButtonUp(1))
            {
                OnInputAction?.Invoke(EInputAction.RIGHT_CLICK_KEY_UP);
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                OnInputAction?.Invoke(EInputAction.JUMP_KEY_DOWN);
            }
            else if (Input.GetKeyUp(KeyCode.Space))
            {
                OnInputAction?.Invoke(EInputAction.JUMP_KEY_UP);
            }

            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                OnInputAction?.Invoke(EInputAction.CTRL_KEY_DOWN);
            }
            else if (Input.GetKeyUp(KeyCode.LeftControl))
            {
                OnInputAction?.Invoke(EInputAction.CTRL_KEY_UP);
            }

            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                OnInputAction?.Invoke(EInputAction.SHIFT_KEY_DOWN);
            }
            else if (Input.GetKeyUp(KeyCode.LeftShift))
            {
                OnInputAction?.Invoke(EInputAction.SHIFT_KEY_UP);
            }

            if (Input.GetKeyDown(KeyCode.C))
            {
                OnInputAction?.Invoke(EInputAction.C_KEY_DOWN);
            }
            else if (Input.GetKeyUp(KeyCode.C))
            {
                OnInputAction?.Invoke(EInputAction.C_KEY_UP);
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                OnInputAction?.Invoke(EInputAction.E_KEY_DOWN);
            }
            else if (Input.GetKeyUp(KeyCode.E))
            {
                OnInputAction?.Invoke(EInputAction.E_KEY_UP);
            }

            if (Input.GetKeyDown(KeyCode.Tab))
            {
                OnInputAction?.Invoke(EInputAction.TAB_KEY_DOWN);
            }
            else if (Input.GetKeyUp(KeyCode.Tab))
            {
                OnInputAction?.Invoke(EInputAction.TAB_KEY_UP);
            }

            if (Input.GetKeyDown(KeyCode.Return))
            {
                OnInputAction?.Invoke(EInputAction.ENTER_KEY_DOWN);
            }
            else if (Input.GetKeyUp(KeyCode.Return))
            {
                OnInputAction?.Invoke(EInputAction.ENTER_KEY_UP);
            }

            if (Input.GetKeyDown(KeyCode.V))
            {
                OnInputAction?.Invoke(EInputAction.V_KEY_DOWN);
            }
            else if (Input.GetKeyUp(KeyCode.V))
            {
                OnInputAction?.Invoke(EInputAction.V_KEY_UP);
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                OnInputAction?.Invoke(EInputAction.ESC_KEY_DOWN);
            }
            else if (Input.GetKeyUp(KeyCode.Escape))
            {
                OnInputAction?.Invoke(EInputAction.ESC_KEY_UP);
            }

            OnInputValueUpdated?.Invoke(EInputValue.VERTICAL, Input.GetAxis("Vertical"));
            OnInputValueUpdated?.Invoke(EInputValue.HORIZONTAL, Input.GetAxis("Horizontal"));
            OnMouseValueUpdated?.Invoke(EMouseValue.PITCH, Input.GetAxis("Mouse X"));
            OnMouseValueUpdated?.Invoke(EMouseValue.YAW, Input.GetAxis("Mouse Y"));

            SwitchItemSlot();
        }

        #region ITEM SLOT

        private int _slot;

        private void SwitchItemSlot()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                if (_slot == 0) return;
                CmdSetItemInHand(0);
                _slot = 0;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                if (_slot == 1) return;
                CmdSetItemInHand(1);
                _slot = 1;
            }
            else if (Input.GetKeyDown(KeyCode.Q))
            {
                _slot = _slot == 0 ? 1 : 0;
                CmdSetItemInHand(_slot);
            }
        }

        [Command]
        private void CmdSetItemInHand(int slot)
        {
            GetComponent<Player>().SetHeldItemSlot(slot);
        }

        #endregion
    }
}