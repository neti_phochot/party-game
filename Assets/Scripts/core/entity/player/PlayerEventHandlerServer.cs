﻿using core.interact;
using Mirror;
using UnityEngine;
using utils;

namespace core.entity.player
{
    public class PlayerEventHandlerServer : NetworkBehaviour
    {
        public override void OnStartServer()
        {
            base.OnStartServer();

            PlayerInteract playerInteract = GetComponent<PlayerInteract>();
            playerInteract.OnPlayerUseItem += OnPlayerUseItemEvent;
            playerInteract.OnPlayerInteracted += OnPlayerInteractedEvent;
        }

        public override void OnStopServer()
        {
            base.OnStopServer();

            PlayerInteract playerInteract = GetComponent<PlayerInteract>();
            playerInteract.OnPlayerUseItem -= OnPlayerUseItemEvent;
            playerInteract.OnPlayerInteracted -= OnPlayerInteractedEvent;
        }

        //------------------------------
        // GLOBAL ITEM EVENT
        //------------------------------
        [Server]
        private void OnPlayerUseItemEvent(Player p, ClickActionType clickActionType)
        {
            if (clickActionType != ClickActionType.DOWN) return;

            if (p.GetItemInHand().material == Material.AIR) //Punch
            {
                p.SetTargetAction(1, (other) =>
                {
                    Vector3 pushDir = (other.GetLocation() - p.GetLocation()).normalized;
                    other.AddVelocity(pushDir * 10f);
                });
            }
            //else if (p.GetItemInHand().material == Material.KNIFE)
            //{
            //    p.SetTargetAction(1, (other) =>
            //    {
            //        Vector3 pushDir = (other.GetLocation() - p.GetLocation()).normalized;
            //        other.AddVelocity(pushDir * 5f);
            //        other.Damage(50f, DeathEffect.RAGDOLL);
            //    });
            //}
        }

        [Server]
        private void OnPlayerInteractedEvent(Player player, ClickActionType clickActionType)
        {
            if (clickActionType != ClickActionType.DOWN) return;
            foreach (var interactableObject in player.GetNearByInteractableObjects(1, 1.7f, 70f))
            {
                interactableObject.Init(player, clickActionType);
                interactableObject.Interact();
            }
        }
    }
}