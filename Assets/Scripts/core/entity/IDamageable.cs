﻿namespace core.entity
{
    public interface IDamageable
    {
        void Damage(float damage, DeathEffect deathEffect);
    }
}