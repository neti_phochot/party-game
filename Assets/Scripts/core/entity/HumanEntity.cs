﻿using System;
using System.Collections.Generic;
using System.Linq;
using core.interact;
using core.inventory;
using core.inventory.item;
using Mirror;
using UnityEngine;

namespace core.entity
{
    public abstract class HumanEntity : NetworkBehaviour, ILivingEntity, IPlayerInventory
    {
        [Header("HUMAN ENTITY")] [SerializeField]
        private LayerMask entityLayerMask;

        [SerializeField] private LayerMask interactableLayerMask;

        private const int INVENTORY_SIZE = 2;
        private const int ARMOR_SIZE = 4;

        private readonly SyncList<ItemStack> inventory = new SyncList<ItemStack>();
        private readonly SyncList<ItemStack> armor = new SyncList<ItemStack>();

        [SyncVar(hook = nameof(OnHealthChangedEvent))] [SerializeField]
        private float health;

        [SyncVar(hook = nameof(OnMaxHealthChangedEvent))] [SerializeField]
        private float maxHealth;

        [SyncVar(hook = nameof(OnHeldItemSlotChangedEvent))]
        private int _heldItemSlot;

        [SyncVar(hook = nameof(OnPlayerDeathEvent))]
        private DeathEffect deathEffect = DeathEffect.NONE;

        //EVENT
        public event Action<HumanEntity, DeathEffect> OnPlayerDeath;
        public event Action<HumanEntity, float, float> OnHealthChanged;
        public event Action<HumanEntity, float, float> OnMaxHealthChanged;
        public event Action<HumanEntity, int, int> OnHeldItemSlotChanged;
        public event Action<HumanEntity, ItemStack> OnSwapHandItem;
        public event Action<HumanEntity, ItemStack, ArmorType> OnArmorChanged;

        protected virtual void Awake()
        {
        }

        public override void OnStartServer()
        {
            base.OnStartServer();
            InitInventory();
            InitArmor();
        }

        private void InitInventory()
        {
            for (int i = 0; i < INVENTORY_SIZE; i++)
            {
                inventory.Add(ItemStack.CreateItem(Material.AIR));
            }
        }

        private void InitArmor()
        {
            for (int i = 0; i < ARMOR_SIZE; i++)
            {
                armor.Add(ItemStack.CreateItem(Material.AIR));
            }
        }

        public override void OnStartClient()
        {
            base.OnStartClient();
            inventory.Callback += OnInventoryUpdatedEvent;
            armor.Callback += OnArmorUpdatedEvent;
        }

        public void Damage(float damage, DeathEffect deathEffect = DeathEffect.BLOOD)
        {
            if (IsDead()) return;
            if (damage < 0f) return;
            health -= damage;
            if (health > 0f) return;
            OnDeath(deathEffect);
        }

        protected virtual void OnDeath(DeathEffect deathEffect) => this.deathEffect = deathEffect;
        public float GetHealth() => health;
        public void SetHealth(float value) => health = value;
        public float GetMaxHealth() => maxHealth;
        public void SetMaxHealth(float value) => maxHealth = value;
        public bool IsDead() => deathEffect != DeathEffect.NONE;

        public List<IEntity> GetNearByEntities(int size, float checkRadius, float angle = 180f)
        {
            size += 1;
            Collider[] hitColliders = new Collider[size];
            Physics.OverlapSphereNonAlloc(GetLocation(), checkRadius, hitColliders, entityLayerMask);

            List<IEntity> result = new List<IEntity>();
            foreach (var c in hitColliders)
            {
                if (!c || c.TryGetComponent<HumanEntity>(out var humanEntity) && humanEntity == this) continue;
                Vector3 dirToTarget = (c.transform.position - GetLocation()).normalized;
                float angleToTarget = Vector3.Angle(GetLookDirection(), dirToTarget);
                if (angleToTarget > angle) continue;
                result.Add(humanEntity);
            }

            return result;
        }

        public List<InteractableObject> GetNearByInteractableObjects(int size, float checkRadius, float angle = 180f)
        {
            Collider[] hitColliders = new Collider[size];
            Physics.OverlapSphereNonAlloc(GetLocation(), checkRadius, hitColliders, interactableLayerMask);

            List<InteractableObject> result = new List<InteractableObject>();
            foreach (var c in hitColliders)
            {
                if (!c || !c.TryGetComponent<InteractableObject>(out var interactableObject)) continue;
                Vector3 dirToTarget = (c.transform.position - GetLocation()).normalized;
                float angleToTarget = Vector3.Angle(GetLookDirection(), dirToTarget);
                if (angleToTarget > angle) continue;
                result.Add(interactableObject);
            }

            return result;
        }

        public Vector3 GetLookDirection() => transform.forward;
        public Vector3 GetLocation() => transform.position;
        public void SetLocation(Vector3 location) => transform.position = location;
        public GameObject GetIdentity() => gameObject;
        public ItemStack[] GetContents() => inventory.ToArray();
        public ItemStack GetItem(int slot) => inventory[slot];
        public void SetItem(ItemStack itemStack, int slot) => inventory[slot] = itemStack;
        public void SetItem(Material material, int slot) => inventory[slot] = ItemStack.CreateItem(material);
        public bool HasItem(Material material) => inventory.Any(item => item.material == material);
        public int GetInventorySize() => INVENTORY_SIZE;
        public int GetHeldItemSlot() => _heldItemSlot;
        public void SetHeldItemSlot(int slot) => _heldItemSlot = slot;
        public ItemStack GetItemInHand() => inventory[_heldItemSlot];
        public void SetItemInHand(Material material) => SetItemInHand(ItemStack.CreateItem(material));
        public void SetItemInHand(ItemStack itemStack) => inventory[_heldItemSlot] = itemStack;

        #region ARMOR

        public ItemStack GetHelmet() => armor[0];
        public ItemStack GetChestplate() => armor[1];
        public ItemStack GetLeggings() => armor[2];
        public ItemStack GetBoots() => armor[3];
        public void SetHelmet(ItemStack itemStack) => armor[0] = itemStack;
        public void SetChestplate(ItemStack itemStack) => armor[1] = itemStack;
        public void SetLeggings(ItemStack itemStack) => armor[2] = itemStack;
        public void SetBoots(ItemStack itemStack) => armor[3] = itemStack;
        public void SetHelmet(Material material) => SetHelmet(ItemStack.CreateItem(material));
        public void SetChestplate(Material material) => SetChestplate(ItemStack.CreateItem(material));
        public void SetLeggings(Material material) => SetLeggings(ItemStack.CreateItem(material));
        public void SetBoots(Material material) => SetBoots(ItemStack.CreateItem(material));

        #endregion

        #region EVENT

        //-----------------------------------------
        // SYNCVAR HOOK CALLBACK
        //-----------------------------------------

        private void OnPlayerDeathEvent(DeathEffect old, DeathEffect newValue) =>
            OnPlayerDeath?.Invoke(this, newValue);

        private void OnHealthChangedEvent(float oldHealth, float currentHealth) =>
            OnHealthChanged?.Invoke(this, oldHealth, currentHealth);

        private void OnMaxHealthChangedEvent(float oldHealth, float currentHealth) =>
            OnMaxHealthChanged?.Invoke(this, oldHealth, currentHealth);

        private void OnHeldItemSlotChangedEvent(int oldSlot, int newSlot) =>
            OnHeldItemSlotChanged?.Invoke(this, oldSlot, newSlot);

        private void OnSwapHandItemEvent(ItemStack oldItemStack, ItemStack currentItemStack) =>
            OnSwapHandItem?.Invoke(this, currentItemStack);

        //-----------------------------------------
        // SYNCLIST HOOK CALLBACK
        //-----------------------------------------
        private void OnInventoryUpdatedEvent(
            SyncList<ItemStack>.Operation op, int itemindex, ItemStack olditem, ItemStack newitem)
        {
            if (_heldItemSlot != itemindex) return;
            OnSwapHandItemEvent(olditem, newitem);
        }

        private void OnArmorUpdatedEvent(SyncList<ItemStack>.Operation op, int itemindex, ItemStack olditem,
            ItemStack newitem)
        {
            ArmorType[] armorTypes = (ArmorType[])Enum.GetValues(typeof(ArmorType));
            OnArmorChanged?.Invoke(this, newitem, armorTypes[itemindex]);
        }

        #endregion
    }
}