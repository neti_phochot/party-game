﻿using core.inventory.item;

namespace core.inventory
{
    public interface IPlayerInventory : IInventory
    {
        int GetHeldItemSlot();
        void SetHeldItemSlot(int slot);
        ItemStack GetItemInHand();
        void SetItemInHand(ItemStack itemStack);
        void SetItemInHand(Material material);
        
        ItemStack GetHelmet();
        ItemStack GetChestplate();
        ItemStack GetLeggings();
        ItemStack GetBoots();
        
        void SetHelmet(ItemStack itemStack);
        void SetChestplate(ItemStack itemStack);
        void SetLeggings(ItemStack itemStack);
        void SetBoots(ItemStack itemStack);
        
        void SetHelmet(Material material);
        void SetChestplate(Material material);
        void SetLeggings(Material material);
        void SetBoots(Material material);
    }
}