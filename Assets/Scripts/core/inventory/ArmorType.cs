﻿namespace core.inventory
{
    public enum ArmorType
    {
        HELMET,
        CHESTPLATE,
        LEGGINGS,
        BOOTS,
    }
}