﻿using core.inventory.item;

namespace core.inventory
{
    public interface IInventory
    {
        ItemStack[] GetContents();
        ItemStack GetItem(int slot);
        void SetItem(ItemStack itemStack, int slot);
        void SetItem(Material material, int slot);
        bool HasItem(Material material);
        int GetInventorySize();
    }
}