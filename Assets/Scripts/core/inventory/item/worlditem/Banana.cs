﻿using core.interact;
using scriptsableobject;
using sfx;
using UnityEngine;
using utils;

namespace core.inventory.item.worlditem
{
    public class Banana : WorldItem
    {
        [Header("SFX")]
        [SerializeField] private EnumValue bananaFX;
        public override void Interact()
        {
            if (clickActionType != ClickActionType.DOWN) return;

            if (player.isLocalPlayer)
            {
                CameraShakeInstance.Instance.Shake(3, 0.25f, 0.2f);
            }
            
            SoundInstance.Instance.PlaySound(bananaFX, transform.position);
        }
    }
}