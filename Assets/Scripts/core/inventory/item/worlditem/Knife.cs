﻿using core.interact;
using scriptsableobject;
using sfx;
using UnityEngine;
using utils;

namespace core.inventory.item.worlditem
{
    public class Knife : WorldItem
    {
        [Header("SFX")] [SerializeField] private EnumValue slashFX;

        public override void Interact()
        {
            if (clickActionType != ClickActionType.DOWN) return;

            if (player.isLocalPlayer)
            {
                CameraShakeInstance.Instance.Shake(3, 0.25f, 0.2f);
            }

            SoundInstance.Instance.PlaySound(slashFX, transform.position);
        }
    }
}