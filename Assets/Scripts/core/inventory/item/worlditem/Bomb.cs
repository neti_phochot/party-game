﻿
using core.interact;
using UnityEngine;
using utils;

namespace core.inventory.item.worlditem
{
    public class Bomb : WorldItem
    {
        public override void Interact()
        {
            if (clickActionType != ClickActionType.DOWN) return;

            if (player.isLocalPlayer)
            {
                CameraShakeInstance.Instance.Shake(3, 0.25f, 0.2f);
            }

        }
    }
}