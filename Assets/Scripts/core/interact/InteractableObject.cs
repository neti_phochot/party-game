﻿using core.entity.player;
using UnityEngine;

namespace core.interact
{
    public abstract class InteractableObject : MonoBehaviour, IInteractable
    {
        protected Player player;
        protected ClickActionType clickActionType;

        public void Init(Player p, ClickActionType clickAction)
        {
            player = p;
            clickActionType = clickAction;
        }

        public abstract void Interact();
    }
}