﻿namespace pool
{
    public interface IPool<T>
    {
        void Prewarm(int init);
        T Request();
        void Return(T member);
    }
}