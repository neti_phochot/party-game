﻿using System;
using Mirror;
using Steamworks;
using UnityEngine;

namespace networking
{
    public class MatchManager : MonoBehaviour
    {
        public static MatchManager Instance { get; private set; }

        public struct LobbySetting
        {
            public string lobbyName;
            public ELobbyType lobbyType;
            public int maxPlayer;
            public string inviteCode;
        }

        public static readonly int INVITE_CODE_LENGTH = 5;

        public static readonly string HOST_ADDRESS_KEY = "host_address_key";
        public static readonly string LOBBY_NAME_KEY = "lobby_name_key";
        public static readonly string INVITE_CODE_KEY = "invite_code_key";
        public static readonly string MATCH_VERSION_KEY = "match_version_key";
        public static string MATCH_VERSION_VALUE;

        public static event Action<CSteamID[]> OnLobbiesFound;
        public static event Action<CSteamID> OnInviteLobbyFound;

        protected Callback<LobbyCreated_t> lobbyCreated;
        protected Callback<LobbyEnter_t> lobbyEnter;
        protected Callback<GameLobbyJoinRequested_t> gameLobbyJoinRequested;
        protected Callback<LobbyMatchList_t> lobbyMatchList;

        private LobbySetting _lobbySetting;

        private bool _useInviteCode;

        private void Awake()
        {
            Instance = this;

            MATCH_VERSION_VALUE = Application.version;
        }

        private void OnEnable()
        {
            if (!SteamManager.Initialized) return;

            lobbyCreated = Callback<LobbyCreated_t>.Create(OnLobbyCreated);
            lobbyEnter = Callback<LobbyEnter_t>.Create(OnLobbyEnter);
            gameLobbyJoinRequested = Callback<GameLobbyJoinRequested_t>.Create(OnGameLobbyJoinRequested);
            lobbyMatchList = Callback<LobbyMatchList_t>.Create(OnLobbyMatchList);
        }

        private void OnLobbyCreated(LobbyCreated_t callback)
        {
            if (callback.m_eResult != EResult.k_EResultOK)
            {
                Debug.Log($"[{nameof(MatchManager)}] Failed to create lobby!");
                return;
            }

            CSteamID lobbyId = new CSteamID(callback.m_ulSteamIDLobby);
            SteamMatchmaking.SetLobbyData(lobbyId, HOST_ADDRESS_KEY, SteamUser.GetSteamID().ToString());
            SteamMatchmaking.SetLobbyData(lobbyId, LOBBY_NAME_KEY, _lobbySetting.lobbyName);
            SteamMatchmaking.SetLobbyData(lobbyId, INVITE_CODE_KEY, _lobbySetting.inviteCode);
            SteamMatchmaking.SetLobbyData(lobbyId, MATCH_VERSION_KEY, MATCH_VERSION_VALUE);

            MyNetworkManager.Instance.maxConnections = _lobbySetting.maxPlayer;
            MyNetworkManager.Instance.LobbyId = (ulong)lobbyId;
            MyNetworkManager.Instance.StartHost();
        }


        private void OnLobbyEnter(LobbyEnter_t callback)
        {
            //Set invite Code
            CSteamID lobbyId = new CSteamID(callback.m_ulSteamIDLobby);
            MyNetworkManager.Instance.InviteCode = SteamMatchmaking.GetLobbyData(lobbyId, INVITE_CODE_KEY);
            MyNetworkManager.Instance.ServerName = SteamMatchmaking.GetLobbyData(lobbyId, LOBBY_NAME_KEY);

            if (NetworkServer.active)
            {
                return;
            }

            //Client
            MyNetworkManager.Instance.networkAddress = SteamMatchmaking.GetLobbyData(lobbyId, HOST_ADDRESS_KEY);
            MyNetworkManager.Instance.StartClient();
        }

        private void OnGameLobbyJoinRequested(GameLobbyJoinRequested_t callback)
        {
            //Friend Invite
            JoinGame(callback.m_steamIDLobby);
        }

        private void OnLobbyMatchList(LobbyMatchList_t callback)
        {
            if (_useInviteCode) LoadInviteServer();
            else LoadServerList(callback.m_nLobbiesMatching);
        }

        private void LoadInviteServer()
        {
            CSteamID lobbyId = SteamMatchmaking.GetLobbyByIndex(0);
            OnInviteLobbyFound?.Invoke(lobbyId);
        }

        private void LoadServerList(uint lobbiesMatching)
        {
            CSteamID[] foundLobbies = new CSteamID[lobbiesMatching];
            for (int i = 0; i < lobbiesMatching; i++)
            {
                CSteamID lobbyId = SteamMatchmaking.GetLobbyByIndex(i);
                foundLobbies[i] = lobbyId;
            }

            OnLobbiesFound?.Invoke(foundLobbies);
        }

        public void HostGame(LobbySetting lobbySetting)
        {
            _lobbySetting = lobbySetting;
            SteamMatchmaking.CreateLobby(_lobbySetting.lobbyType, _lobbySetting.maxPlayer);
        }

        public void JoinGame(CSteamID lobbyId)
        {
            SteamMatchmaking.JoinLobby(lobbyId);
        }

        public void FindGame(int maxSearch, ELobbyDistanceFilter eLobbyDistanceFilter)
        {
            _useInviteCode = false;

            SteamMatchmaking.AddRequestLobbyListStringFilter(
                MATCH_VERSION_KEY, MATCH_VERSION_VALUE, ELobbyComparison.k_ELobbyComparisonEqual);

            SteamMatchmaking.AddRequestLobbyListDistanceFilter(eLobbyDistanceFilter);
            SteamMatchmaking.AddRequestLobbyListResultCountFilter(maxSearch);
            SteamMatchmaking.RequestLobbyList();
        }

        public void FindGame(string inviteCode, int maxSearch, ELobbyDistanceFilter eLobbyDistanceFilter)
        {
            _useInviteCode = true;

            inviteCode = inviteCode.ToUpper();
            SteamMatchmaking.AddRequestLobbyListStringFilter(
                INVITE_CODE_KEY, inviteCode, ELobbyComparison.k_ELobbyComparisonEqual);

            SteamMatchmaking.AddRequestLobbyListStringFilter(
                MATCH_VERSION_KEY, MATCH_VERSION_VALUE, ELobbyComparison.k_ELobbyComparisonEqual);

            SteamMatchmaking.AddRequestLobbyListDistanceFilter(eLobbyDistanceFilter);
            SteamMatchmaking.AddRequestLobbyListResultCountFilter(maxSearch);
            SteamMatchmaking.RequestLobbyList();
        }
    }
}