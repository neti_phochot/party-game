﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using assets;
using core.entity.player;
using game.gameplayer;
using Mirror;
using networking.setup;
using Steamworks;
using ui.loadingscreen;
using UnityEngine;
using Random = UnityEngine.Random;

namespace networking
{
    public class MyNetworkManager : NetworkManager
    {
        private const int LOAD_GAME_SCENE_DELAY = 6;
        private const int LOAD_WINNER_SCENE_DELAY = 1;

        private static MyNetworkManager _i;

        public static MyNetworkManager Instance
        {
            get
            {
                if (!_i)
                {
                    _i = singleton as MyNetworkManager;
                }

                return _i;
            }
        }

        public enum GameState
        {
            LOBBY,
            PREGAME,
            INGAME,
            ENDING,
            CELEBRATING,
        }

        public enum RoomType
        {
            ANY,
            LOBBY,
            GAME,
            CELEBRATING,
        }


        [Serializable]
        public struct NetworkPrefabData
        {
            public RoomType[] roomTypes;
            public NetworkBehaviour networkBehaviour;
        }

        [Header("PLAYER PREFAB")] [SerializeField]
        private NetworkPrefabData[] networkPlayerPrefab;

        [Header("ROOM COMPONENT PREFAB")] [SerializeField]
        private NetworkPrefabData[] networkComponentPrefab;

        [Header("DEFAULT SETTINGS")] [SerializeField]
        private string defaultPlayerStatus;

        public string Winner { get; private set; }
        public static event Action<GamePlayer> OnPlayerStatusChanged;
        public static event Action<GamePlayer> OnPlayerJoined;
        public static event Action<GamePlayer> OnPlayerQuit;

        private readonly Dictionary<int, GamePlayer> _players = new Dictionary<int, GamePlayer>();
        public Dictionary<int, GamePlayer> GetOnlineGamePlayers() => _players;

        public IEnumerable<Player> GetOnlinePlayers() =>
            _players.Values.Select(gamePlayer => gamePlayer.playerGameObject.GetComponent<Player>());

        private List<int> _qualifiedPlayers = new List<int>();

        private GameState _gameState = GameState.LOBBY;

        private int _currentMapIndex;
        private LoadingScreenType _loadingScreenType;
        private int _loadSceneDelay;

        private bool _isLoadingScene;

        //SteamNetworking

        public ulong LobbyId { get; set; }
        public string ServerName { get; set; }
        public string InviteCode { get; set; }

        public override void Awake()
        {
            base.Awake();
            LoadGameMap();
        }

        public override void OnServerAddPlayer(NetworkConnection conn)
        {
            InitNetworkPrefabData(networkPlayerPrefab, conn);
            AddGamePlayer(conn);
        }

        public override void OnServerDisconnect(NetworkConnection conn)
        {
            RemoveGamePlayer(conn);
            base.OnServerDisconnect(conn);
        }

        public override void OnServerChangeScene(string newSceneName)
        {
            base.OnServerChangeScene(newSceneName);
            ResetPlayer();
        }

        public override void OnServerSceneChanged(string sceneName)
        {
            base.OnServerSceneChanged(sceneName);
            InitNetworkPrefabData(networkComponentPrefab);
            SendLoadingScene(false, _loadSceneDelay, _currentMapIndex, _loadingScreenType);
            _isLoadingScene = false;
        }

        private void InitNetworkPrefabData(NetworkPrefabData[] networkPrefabData, NetworkConnection conn = default)
        {
            foreach (var data in networkPrefabData)
            {
                foreach (var roomType in data.roomTypes)
                {
                    bool isSpawn = roomType switch
                    {
                        RoomType.LOBBY => IsLobbyRoom(),
                        RoomType.GAME => IsGameRoom(),
                        RoomType.CELEBRATING => IsCelebratingRoom(),
                        _ => true
                    };

                    if (!isSpawn) continue;
                    if (conn != null)
                    {
                        //Player Prefab
                        Transform startPos = GetStartPosition();
                        GameObject player = startPos != null
                            ? Instantiate(data.networkBehaviour.gameObject, startPos.position, startPos.rotation)
                            : Instantiate(data.networkBehaviour.gameObject);
                        NetworkServer.AddPlayerForConnection(conn, player);
                        return;
                    }

                    //Scene Component Prefab
                    GameObject componentObj = Instantiate(data.networkBehaviour.gameObject);
                    NetworkServer.Spawn(componentObj);
                    break;
                }
            }
        }

        public bool IsLobbyRoom() => _currentMapIndex == 0;
        public bool IsCelebratingRoom() => _currentMapIndex == 1;
        public bool IsGameRoom() => _currentMapIndex > 1;

        #region PLAYER MANAGER

        private void ResetPlayer()
        {
            _players.Clear();
        }

        private void AddGamePlayer(NetworkConnection conn)
        {
            if (_players.ContainsKey(conn.connectionId)) return;
            GameObject playerGameObjectObject = conn.identity.gameObject;

            GamePlayer gamePlayer = new GamePlayer
            {
                status = defaultPlayerStatus,
                playerGameObject = playerGameObjectObject,
            };

            _players.Add(conn.connectionId, gamePlayer);

            //Setup Player
            Player player = gamePlayer.ToPlayer();
            if (player)
            {
                player.SetConnectionId(conn.connectionId);
                player.SetDisplayName($"id[{conn.connectionId}]");
            }

            OnPlayerJoined?.Invoke(gamePlayer);
        }

        private void RemoveGamePlayer(NetworkConnection conn)
        {
            if (!_players.ContainsKey(conn.connectionId)) return;
            GamePlayer gamePlayer = _players[conn.connectionId];
            _players.Remove(conn.connectionId);
            OnPlayerQuit?.Invoke(gamePlayer);
        }

        public void SetGamePlayerStatus(int connectionId, string playerStatus)
        {
            GamePlayer gamePlayer = _players[connectionId];
            gamePlayer.status = playerStatus;
            _players[connectionId] = gamePlayer;
            OnPlayerStatusChanged?.Invoke(gamePlayer);
        }

        #endregion

        #region SERVER MAP

        private readonly Queue<int> _playedMap = new Queue<int>();
        private List<int> _mapList = new List<int>();
        public void SetGameState(GameState gameState) => _gameState = gameState;
        public GameState GetGameState() => _gameState;

        private bool IsGameMap(int mapId) => mapId > 1;

        public async void LoadGameMap(int index, int loadSceneDelay)
        {
            if (_isLoadingScene) return;
            _isLoadingScene = true;

            MapAssetSO mapAssetSo = GameAssetInstance.Instance.MapAssetSo;
            if (index > mapAssetSo.MapCount) return;

            if (IsGameMap(index))
            {
                //Played map history
                _playedMap.Enqueue(index);
            }
            else
            {
                ClearQualifiedPlayer();
            }

            _currentMapIndex = index;
            _loadSceneDelay = loadSceneDelay;
            switch (index)
            {
                case 0:
                    SetGameState(GameState.LOBBY);
                    _loadingScreenType = LoadingScreenType.GAMEMAP;
                    break;
                case 1:
                    SetGameState(GameState.CELEBRATING);
                    _loadingScreenType = LoadingScreenType.CELEBRATING;
                    break;
                default:
                    SetGameState(GameState.PREGAME);
                    _loadingScreenType = LoadingScreenType.GAMEMAP;
                    break;
            }

            MapAssetSO.GameMap gameMap = mapAssetSo.GetGameMap(index);

            SendLoadingScene(true, loadSceneDelay, _currentMapIndex, _loadingScreenType);
            await Task.Delay(loadSceneDelay * 1000);
            ChangeScene(gameMap.sceneName);
        }

        public void LoadLobby() => LoadGameMap(0, LOAD_GAME_SCENE_DELAY);

        public void LoadWinner(string winner)
        {
            Winner = winner;
            LoadGameMap(1, LOAD_WINNER_SCENE_DELAY);
        }

        public void LoadRandomMap() => LoadGameMap(GetUnPlayedMapIndex(), LOAD_GAME_SCENE_DELAY);

        private int GetUnPlayedMapIndex()
        {
            List<int> unPlayedMap = new List<int>();
            foreach (var mapIndex in _mapList)
            {
                if (_playedMap.Contains(mapIndex)) continue;
                unPlayedMap.Add(mapIndex);
            }

            if (unPlayedMap.Count > 0)
            {
                return unPlayedMap[Random.Range(0, unPlayedMap.Count)];
            }

            if (_playedMap.Count > 0)
            {
                return _playedMap.Dequeue();
            }

            return 0;
        }

        public void ChangeScene(string sceneName) => ServerChangeScene(sceneName);

        private void SendLoadingScene(bool isLoading, int loadSceneDelay, int mapIndex,
            LoadingScreenType loadingScreenType)
        {
            GameDataManager.Instance.mapData = new GameDataManager.MapData
            {
                loadingScreenType = loadingScreenType,
                isStartLoading = isLoading,
                loadSceneDelay = loadSceneDelay,
                mapIndex = mapIndex,
            };
        }

        private void LoadGameMap()
        {
            _mapList = new List<int>();
            for (int i = 0; i < GameAssetInstance.Instance.MapAssetSo.MapCount; i++)
            {
                if (i == 0 || i == 1) continue;
                _mapList.Add(i);
            }
        }

        #endregion

        #region GAME

        public void ClearQualifiedPlayer()
        {
            _qualifiedPlayers.Clear();
        }

        public void SetQualifyPlayer(List<Player> players)
        {
            players.ForEach(p => _qualifiedPlayers.Add(p.GetConnectionId()));
        }

        public bool IsQualifiedPlayer(Player player)
        {
            return _qualifiedPlayers.Count == 0 || _qualifiedPlayers.Contains(player.GetConnectionId());
        }

        #endregion

        #region TODO

        public void LeaveLobby()
        {
            //Source: NetworkManagerHUD.cs

            // stop host if host mode
            if (NetworkServer.active && NetworkClient.isConnected)
            {
                StopHost();
            }
            // stop client if client-only
            else if (NetworkClient.isConnected)
            {
                StopClient();
            }
            // stop server if server-only
            else if (NetworkServer.active)
            {
                StopServer();
            }

            //SteamNetworking
            if (LobbyId != default)
            {
                if (SteamManager.Initialized)
                {
                    CSteamID lobbyId = (CSteamID)LobbyId;
                    SteamMatchmaking.LeaveLobby(lobbyId);
                }
            }
        }

        #endregion
    }
}