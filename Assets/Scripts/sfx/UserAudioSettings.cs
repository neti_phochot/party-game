﻿using UnityEngine;

namespace sfx
{
    public class UserAudioSettings : MonoBehaviour
    {
        public struct AudioData
        {
            public float Volume;
            public AudioSource AudioSource;
        }

        private AudioData[] _audioData;
        [SerializeField] private UserOptionSO userOptionSo;

        private void Awake()
        {
            userOptionSo.OnMusicOn += OnMusicOnEvent;
            var foundAudioSources = FindObjectsOfType<AudioSource>();
            _audioData = new AudioData[foundAudioSources.Length];

            for (int i = 0; i < foundAudioSources.Length; i++)
            {
                var foundAudio = foundAudioSources[i];
                _audioData[i] = new AudioData
                {
                    Volume = foundAudio.volume,
                    AudioSource = foundAudio,
                };
            }
        }

        private void Start()
        {
            CheckAudio();
        }

        private void OnMusicOnEvent(bool isOn)
        {
            CheckAudio();
        }

        private void CheckAudio()
        {
            foreach (var data in _audioData)
            {
                if (!data.AudioSource) continue;
                data.AudioSource.volume = userOptionSo.IsMusicOn ? data.Volume : 0;
            }
        }
    }
}