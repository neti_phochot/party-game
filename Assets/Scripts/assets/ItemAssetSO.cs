﻿using System;
using core.inventory.item;
using UnityEngine;
using Material = core.Material;

namespace assets
{
    [CreateAssetMenu(fileName = "Item SO", menuName = "Item SO", order = 0)]
    public class ItemAssetSO : ScriptableObject
    {
        [Serializable]
        public struct ItemAsset
        {
            public string itemName;
            public Sprite icon;
            public WorldItem worldItem;
        }

        [SerializeField] private ItemAsset[] items;

        public int ItemAssetCount => items.Length;

        public ItemAsset GetItemAsset(Material material)
        {
            return items[(int)material];
        }
    }
}