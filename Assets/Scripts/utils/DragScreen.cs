﻿using UnityEngine;

namespace utils
{
    public class DragScreen : MonoBehaviour
    {
        //Source: https://forum.unity.com/threads/click-drag-camera-movement.39513/
        [SerializeField] private int mouseButton = 1;
        public float dragSpeed = 2;
        private Vector3 dragOrigin;

        private Camera _cam;

        public Vector2 DragDelta { get; private set; }

        private void Awake()
        {
            _cam = Camera.main;
        }

        void Update()
        {
            if (Input.GetMouseButtonDown(mouseButton))
            {
                dragOrigin = Input.mousePosition;
                return;
            }

            if (!Input.GetMouseButton(mouseButton))
            {
                DragDelta = Vector2.zero;
                return;
            }

            Vector3 pos = _cam.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
            DragDelta = new Vector2(pos.x * dragSpeed, pos.y * dragSpeed);
        }
    }
}