﻿using UnityEngine;

namespace utils
{
    public static class JExtension
    {
        public static string ToRandomLetter(this int letterCount)
        {
            string inviteCode = string.Empty;
            for (int i = 0; i < letterCount; i++)
            {
                int randomNum = Random.Range(1, 36);

                if (randomNum > 26)
                {
                    //Number
                    inviteCode += $"{randomNum - 26}";
                    continue;
                }

                //letter
                char letter = (char)(randomNum + 96);
                inviteCode += letter.ToString().ToUpper();
            }

            return inviteCode;
        }

        public static int GetFrameRate()
        {
            return (int)(1.0f / Time.smoothDeltaTime);
        }
    }
}