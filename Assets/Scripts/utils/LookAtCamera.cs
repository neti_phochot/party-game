﻿using UnityEngine;

namespace utils
{
    public class LookAtCamera : MonoBehaviour
    {
        private Camera _mainCamera;
        private Camera mainCamera
        {
            get
            {
                if (!_mainCamera)
                {
                    _mainCamera = Camera.main;
                }

                return _mainCamera;
            }
        }

        private void FixedUpdate()
        {
            if (!mainCamera) return;
            transform.rotation = Quaternion.LookRotation(mainCamera.transform.forward);
        }
    }
}