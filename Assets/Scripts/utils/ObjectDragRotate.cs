﻿using UnityEngine;

namespace utils
{
    [RequireComponent(typeof(DragScreen))]
    public class ObjectDragRotate : MonoBehaviour
    {
        private DragScreen _dragScreen;

        private void Awake()
        {
            _dragScreen = GetComponent<DragScreen>();
        }

        private void Update()
        {
            _dragScreen.transform.Rotate(new Vector3(0, -(_dragScreen.DragDelta.x)));
        }
    }
}