﻿using System;
using core.entity.player;

namespace utils
{
    public static class GameUtils
    {
        public static void SetTargetAction(this Player player, int maxTarget, Action<Player> action)
        {
            foreach (var entity in player.GetNearByEntities(maxTarget, 1.8f, 110f))
            {
                if (!(entity is Player other)) continue;
                action(other);
            }
        }
    }
}