﻿using UnityEngine;

namespace fx.addon
{
    public class RagdollEffectFix : MonoBehaviour
    {
        [SerializeField] private GameObject ragdollPrefab;

        private void OnEnable()
        {
            foreach (Transform child in transform)
            {
                Destroy(child.gameObject);
            }

            Instantiate(ragdollPrefab, transform);
        }
    }
}